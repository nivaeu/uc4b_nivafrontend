# Introduction
This subproject is part of the ["New IACS Vision in Action” NIVA](https://www.niva4cap.eu/) project that delivers 
a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to 
support further development of IACS that will facilitate data and information flows.

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under 
grant agreement No 842009.

Please visit the [website](https://www.niva4cap.eu) for further information. A complete list of the sub-projects 
made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

# Scripts
- **ng**: Runs Angular CLI commands.
- **start**: Starts a local development server.
- **build**: Builds the app for production.
- **test**: Runs unit tests.
- **lint**: Runs linting on the app.

# NivaFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `https://localhost.vfltest.dk:8080/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit and integration tests

Run `npm run test` to execute the unit tests with [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## License
EU-PL Nest framework is <a rel="nofollow noreferrer noopener" href="https://github.com/nestjs/nest/blob/master/LICENSE">MIT licensed</a>.  

![European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009](NIVA.jpg)
