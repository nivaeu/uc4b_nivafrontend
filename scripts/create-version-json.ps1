$outFile = "../dist/version.json";

$packageJson = Get-Content '../package.json' | Out-String | ConvertFrom-Json

$version = $packageJson.version;
$lastCommit = git log -1;
$lastCommitHash = git log --format="%H" -n 1;
$lastCommitMessage = git log -1 --pretty=%B;
$lastCommitDate = git log -1 --format=%cd;

$obj = @{
    version           = $version;
    lastCommit        = $lastCommit;
    lastCommitHash    = $lastCommitHash;
    lastCommitMessage = $lastCommitMessage;
    lastCommitDate    = $lastCommitDate;
};

$json = $obj | ConvertTo-Json;

if (Test-Path $outFile) {
    Write-Host "Found existing file at $outFile, deleting that...";
    Remove-Item $outFile;
}

Write-Output $json > $outFile;

$fileContent = Get-Content $outFile;

Write-Host "Wrote the following to file ($outFile):"
Write-Host $fileContent;
