param(
  [string]$branch = "develop"
)

$prefix, $name = $branch.Split("/");

Write-Host "Found prefix: $prefix, and name: $name";

if ([string]::IsNullOrEmpty($prefix) -and [string]::IsNullOrEmpty($name)) {
  exit(-1);
}

if (-not [string]::IsNullOrEmpty($name)) {
  Write-Output $name
  exit(0);
}

if (-not [string]::IsNullOrEmpty($prefix)) {
  Write-Output $prefix
  exit(0);
}

exit(-1);
