param(
    [string]$outDir = ".",
    [string]$branch,
    [string]$revision,
    [string]$build
)

$outFile = "$outDir/build.json";
$environment = Invoke-Expression "$PSScriptRoot\get-environment.ps1 -branch $branch";

if ($environment -eq "feature") {
    $branch = Invoke-Expression "$PSScriptRoot\get-branch-without-prefix.ps1 -branch $branch";
}

$obj = @{
    branch      = $branch;
    revision    = $revision;
    environment = $environment;
    build       = $build;
};

$json = $obj | ConvertTo-Json;

if (Test-Path $outFile) {
    Write-Host "Found existing file at $outFile, deleting that...";
    Remove-Item $outFile;
}

Write-Output $json > $outFile;

$fileContent = Get-Content $outFile;

Write-Host "Wrote the following to file ($outFile):"
Write-Host $fileContent;
