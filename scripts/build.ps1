param(
    [string]$fullBranchName = "master"
)

$environment = Invoke-Expression "$PSScriptRoot\get-environment.ps1 -branch $fullBranchName";

if ($environment -eq "feature") {
    $branchName = Invoke-Expression "$PSScriptRoot\get-branch-without-prefix.ps1 -branch $fullBranchName";
    npm run build -- --base-href $branchName
}
else {
    npm run build
}

if ($LASTEXITCODE -ne 0) {
    exit($LASTEXITCODE);
}
