param([string] $jira_user,
    [string] $pass,
    [string] $multipartFile,
    [string] $testplanKey)

# REST-Endpoint
$xrayUrl = "https://jira.seges.dk/rest/raven/1.0/import/execution/junit"

# Read file byte-by-byte
$fileBin = [System.IO.File]::ReadAllBytes($multipartFile)

# Convert byte-array to string
$CODEPAGE = "iso-8859-1" # alternatives are ASCII, UTF-8
$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)

$fileEnc = $enc.GetString($fileBin)


# Create Object for Credentials
$pair = "$($jira_user):$($pass)"
$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
$basicAuthValue = "Basic " + $encodedCreds

# We need a boundary (something random() will do best)
$boundary = [System.Guid]::NewGuid().ToString()

# Linefeed character
$LF = "`r`n"

# Build up URI for the API-call
$uri = $xrayUrl + "?projectKey=CT2&testPlanKey=$testplanKey"

# Build Body for our form-data manually since PS does not support multipart/form-data out of the box
$bodyLines = (
    "--$boundary",
    "Content-Disposition: form-data; name=`"file`"; filename=`"junit.xml`"",
    "Content-Type: multipart/form-data$LF",
    $fileEnc,
    "--$boundary--$LF"
) -join $LF

try {
    # Submit form-data with Invoke-RestMethod-Cmdlet
    Invoke-RestMethod -Uri $uri -Method Post -ContentType "multipart/form-data; boundary=`"$boundary`"" -Body $bodyLines -Headers @{Authorization = $basicAuthValue }
}
# In case of emergency...
catch [System.Net.WebException] {
    Write-Error( "REST-API-Call failed for '$URL': $_" )
    throw $_
}
