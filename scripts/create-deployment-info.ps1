param(
    [string]$outDir = "."
)

$outFile = "$outDir/deploymentinfo.txt";

$lastDeploymentTag = git describe --match "deployment*" --abbrev=0 HEAD

Write-Host "Last deployment: $lastDeploymentTag"

$deploymentInfo = git log "$lastDeploymentTag..HEAD" --oneline

Write-Host "$deploymentInfo";

[System.IO.File]::WriteAllLines($outFile, $deploymentInfo);

$fileContent = Get-Content $outFile;

Write-Host "Wrote the following to file ($outFile):"
Write-Host $fileContent;
