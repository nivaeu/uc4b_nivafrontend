param(
  [string]$branch = "develop"
)

$environment = Invoke-Expression "$PSScriptRoot\get-environment.ps1 -branch $branch";

npm run test:env -- $environment

if ($LASTEXITCODE -ne 0) {
  exit($LASTEXITCODE);
}
