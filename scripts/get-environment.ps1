param(
  [string]$branch = "develop"
)

$environment = "develop";

if ($branch -eq "master") {
  $environment = "master";
} elseif ($branch.StartsWith("feature", "CurrentCultureIgnoreCase")) {
  $environment = "feature";
} elseif ($branch.StartsWith("bugfix", "CurrentCultureIgnoreCase")) {
  $environment = "feature";
} elseif ($branch.StartsWith("release", "CurrentCultureIgnoreCase")) {
  $environment = "release";
} elseif ($branch.StartsWith("hotfix", "CurrentCultureIgnoreCase")) {
  $environment = "release";
}

Write-Host "Using '$environment' environment";
Write-Output $environment;
