const fs = require('fs');
const execSync = require('child_process').execSync;
var currentPath = process.cwd();

fs.readFile(currentPath + '/../build.json', 'utf8', function(err, buildFile) {
  if (err) {
    return console.log(err);
  }
  const buildJson = JSON.parse(buildFile);
  console.log(buildJson);

  const branch = getBranch(buildJson);

  console.log('Branch is: ' + branch);

  if (branch === 'master') {
    console.log('Branch is master, running release script.');
    runRelease();
  } else {
    console.log('Branch is not master, skipping release script.');
  }
});

function getBranch(buildJson) {
  if (!buildJson.branch) {
    console.error('No branch!');
  }

  return buildJson.branch;
}

function runRelease() {
  console.log('Run release script');
  execSync('npm run release', function(err, stdout, stderr) {
    if (err || stderr) {
      console.error('err', err);
      console.error('stderr', stderr);
    }

    console.log(stdout);
  });

  console.log('Git push to origin master');
  execSync('git push --no-verify --follow-tags origin master', function(err, stdout, stderr) {
    if (err || stderr) {
      console.error('err', err);
      console.error('stderr', stderr);
    }

    console.log(stdout);
  });
}
