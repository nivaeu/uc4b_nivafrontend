/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { endpoints } from './endpoints';

declare var window: any;

export const environment = {
  production: false,
  endpoints: endpoints,
  auth: {
    get callerId() {
      return window.config ? window.config.auth.callerId : '';
    },
    get applicationId() {
      return window.config ? window.config.auth.applicationId : '';
    },
  },
  bingMaps: {
    get key() {
      return window.config.bingMaps.key;
    },
  },
  settings: {
    get farmId() {
      return window.config.settings.farmId;
    },
    get harvestYear() {
      return window.config.settings.harvestYear;
    },
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
