/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
declare var window: any;

export class Endpoints {
  public static get plantApi() {
    return window.config.urls.plantApi;
  }
  public static get plantAuthorizationService() {
    return window.config.urls.plantAuthorizationService;
  }
  public static get nivaConnectorApi() {
    return window.config.urls.nivaConnectorApi;
  }
  public static get nivaValidatorApi() {
    return window.config.urls.nivaValidatorApi;
  }
}

export const endpoints = Endpoints;
