/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { TokenComponent } from './components/token/token.component';
import { TokenResolverService } from './core/resolvers/token-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
  },
  {
    // Used for handling redirect from the oauth service post login
    path: 'token',
    resolve: {
      data: TokenResolverService,
    },
    component: TokenComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
