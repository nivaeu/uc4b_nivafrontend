/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { filter, switchMap, tap } from 'rxjs/operators';
import { AuthService } from './core/authentication/auth.service';

declare var window: any;

@Injectable({
  providedIn: 'root',
})
export class AppInitService {
  constructor(private authService: AuthService) {}

  public init() {
    return from(fetch('assets/app-config.json').then((response) => response.json()))
      .pipe(
        tap((config) => this.putConfigOntoWindow(config)),
        switchMap(() => this.authService.beginAuthenticationFlow()),
        filter((isAuthenticated) => isAuthenticated)
      )
      .toPromise()
      .catch((err) => {
        // tslint:disable-next-line:no-console
        console.error('Error during bootstrap');
        throw err;
      });
  }

  private putConfigOntoWindow(config: any) {
    window.config = config;
  }
}
