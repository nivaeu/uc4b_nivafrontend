/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CenterModule } from '../center/center.module';
import { LeftSideModule } from '../left-side/left-side.module';
import { RightSideModule } from '../right-side/right-side.module';
import { MainComponent } from './main.component';

@NgModule({
  declarations: [MainComponent],
  imports: [CommonModule, CenterModule, LeftSideModule, RightSideModule],
  exports: [MainComponent],
})
export class MainModule {}
