/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { LocalStorageService } from '@app/core/local-storage/local-storage.service';
import { TasksService } from '@app/core/tasks/tasks.service';
import { BehaviorSubject, combineLatest, Observable, of, Subject, Subscription } from 'rxjs';
import { catchError, filter, finalize, first, map, switchMap } from 'rxjs/operators';
import { ECropReport } from '../left-side/interfaces/e-crop-report-message.class';
import { NivaECropReceipt, NivaTask } from '../left-side/interfaces/niva-task.class';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainComponent implements OnInit, OnDestroy {
  public tasks$: Observable<NivaTask[]> = of([]);
  public approvedTasks$: Observable<NivaTask[]> = of([]);
  public isToastVisible = false;
  public toastMessage = '';
  public isLoading = false;
  public isValidating$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public selectedTask$: Subject<NivaTask> = new Subject<NivaTask>();
  public eCropReceipt$ = new BehaviorSubject<NivaECropReceipt>(
    new NivaECropReceipt({ Sender: '', Purpose: '', Recipient: '', CropPlot: [] })
  );
  public eCropReport$: BehaviorSubject<ECropReport> = new BehaviorSubject<ECropReport>(null);
  private selectedTask: NivaTask;
  private subscriptions = new Subscription();

  constructor(
    private tasksService: TasksService,
    private changeDetectorRef: ChangeDetectorRef,
    private localStorageService: LocalStorageService
  ) {}

  public ngOnInit(): void {
    this.subscriptions.add(
      combineLatest([this.localStorageService.getNivafarmId$(), this.localStorageService.getNivaHarvestYear$()]).subscribe(
        ([farmId, harvestYear]) => {
          if (farmId === 0) {
            this.toastMessage = 'You must input a valid farmId';
            this.isToastVisible = true;
            this.hideToastMessage();
          } else {
            this.getTasks(farmId, harvestYear);
          }
        }
      )
    );
  }

  public ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  private getTasks(farmId: number, harvestYear: number) {
    this.isLoading = true;
    this.tasks$ = this.tasksService.getTasks(farmId, harvestYear).pipe(finalize(() => (this.isLoading = false)));
  }

  public onTaskSelect(task: NivaTask) {
    this.selectedTask$
      .pipe(
        first(),
        filter((selectedTask) => selectedTask && task.id === selectedTask.id)
      )
      .subscribe(() => {
        return;
      });

    this.isLoading = true;
    this.isValidating$.next(true);

    this.tasksService
      .getECropReport(task.id)
      .pipe(
        first(),
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe((eCropReport) => {
        this.eCropReport$.next(eCropReport);
      });

    this.tasksService
      .checkTaskValidity(task)
      .pipe(
        first(),
        switchMap((validations) => {
          task.validations = validations;
          this.selectedTask$.next(task);
          this.selectedTask = task;
          return this.tasksService.getECropReceipt(task);
        }),
        finalize(() => {
          this.isValidating$.next(false);
        })
      )
      .subscribe((eCropReceipt) => {
        if (eCropReceipt.error) {
          this.isToastVisible = true;
          this.toastMessage = `ErrorCode ${eCropReceipt.error.error.ErrorCode} - ${eCropReceipt.error.error.ErrorMessage}`;
          this.hideToastMessage();
        }
        this.eCropReceipt$.next(eCropReceipt);
      });
  }

  public onApprove() {
    let stopApprove = false;
    this.approvedTasks$.pipe(first()).subscribe((approvedTasks) => {
      if (approvedTasks.find((t) => t.id === this.selectedTask.id)) {
        stopApprove = true;
      }
    });

    if (stopApprove) {
      this.isToastVisible = true;
      this.toastMessage = 'Task is shared with paying agency';
      this.hideToastMessage();
      return;
    }

    if (!this.selectedTask) {
      this.isToastVisible = true;
      this.toastMessage = 'Select a task awaiting sharing';
      this.hideToastMessage();
      return;
    }

    this.isLoading = true;

    this.tasksService
      .sendTaskToPA(this.selectedTask)
      .pipe(
        first(),
        catchError((error) => {
          return of(error);
        })
      )
      .subscribe((nivaEcropReturnMessage) => {
        // error in response from paying agency
        if (nivaEcropReturnMessage.returnMessage === undefined) {
          this.toastMessage = `ErrorCode ${nivaEcropReturnMessage.error.ErrorCode} - ${nivaEcropReturnMessage.error.ErrorMessage}`;
        } else {
          this.toastMessage = nivaEcropReturnMessage.returnMessage;
          this.tasks$ = this.tasks$.pipe(map((tasks) => tasks.filter((t) => t.id !== this.selectedTask.id)));
          this.approvedTasks$ = this.approvedTasks$.pipe(map((approvedTasks) => [...approvedTasks, this.selectedTask]));
        }
        this.isLoading = false;
        this.isToastVisible = true;
        this.changeDetectorRef.detectChanges();
        this.hideToastMessage();
      });
  }

  private hideToastMessage() {
    setTimeout(() => {
      this.isToastVisible = false;
      this.toastMessage = '';
      this.changeDetectorRef.detectChanges();
    }, 10000);
  }
}
