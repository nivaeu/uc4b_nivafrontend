/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ECropReport } from '@app/components/left-side/interfaces/e-crop-report-message.class';
import { NivaECropReceipt, NivaTask } from '@app/components/left-side/interfaces/niva-task.class';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ValidationComponent {
  @Input() public task$: Observable<NivaTask>;
  @Input() public eCropReceipt$: Observable<NivaECropReceipt>;
  @Input() public eCropReport$: Observable<ECropReport>;

  public fieldArea$() {
    return this.eCropReport$.pipe(
      map(
        (eCropReport) =>
          eCropReport.eCropReportMessage.agriculturalProducerParty.agriculturalProductionUnit.specifiedCropPlots[0].areaMeasure
      )
    );
  }
}
