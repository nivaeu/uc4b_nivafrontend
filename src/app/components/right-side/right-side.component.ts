/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { ECropReport } from '../left-side/interfaces/e-crop-report-message.class';
import { NivaECropReceipt, NivaTask } from '../left-side/interfaces/niva-task.class';

@Component({
  selector: 'app-right-side',
  templateUrl: './right-side.component.html',
  styleUrls: ['./right-side.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RightSideComponent {
  @Input() public task$: Observable<NivaTask>;
  @Input() public eCropReceipt$: Observable<NivaECropReceipt>;
  @Input() public eCropReport$: Observable<ECropReport>;
  @Input() public isValidating$: Observable<boolean>;
  @Output() public approve = new EventEmitter<void>();

  public onSendClick() {
    this.approve.emit();
  }
}
