/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RightSideComponent } from './right-side.component';
import { ValidationComponent } from './validation/validation.component';

@NgModule({
  declarations: [RightSideComponent, ValidationComponent],
  imports: [CommonModule],
  exports: [RightSideComponent],
})
export class RightSideModule {}
