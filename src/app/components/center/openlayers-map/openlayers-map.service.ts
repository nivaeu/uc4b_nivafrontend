/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Injectable } from '@angular/core';
import { defaults } from 'ol/control';
import Map from 'ol/Map';
import { fromLonLat } from 'ol/proj';
import View from 'ol/View';

@Injectable()
export class OpenlayersMapService {
  constructor() {}

  public createMap(mapElement: any) {
    const olMap = new Map({
      target: mapElement,
      layers: [],
      view: new View({
        center: fromLonLat([10, 56]),
        zoom: 1,
      }),
      controls: defaults({
        rotate: false,
        zoom: false,
      }),
    });

    return olMap;
  }
}
