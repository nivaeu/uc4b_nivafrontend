/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */

export class ScaleLegendItem {
  public color: string;
  public text: string;

  constructor(color: string, text: string) {
    this.color = color;
    this.text = text;
  }
}

export class ScaleSummaryLine {
  public description: string;
  public quantity: number;
  public unit: string;

  constructor(description: string, quantity: number, unit: string) {
    this.description = description;
    this.quantity = quantity;
    this.unit = unit;
  }
}

export class LegendSettings {
  public items: ScaleLegendItem[] = [];
  public note = '';
  public summaryLines: ScaleSummaryLine[] = [];
  public title = '';
  public visible = false;
  public unit = '';
}

export class LegendRange {
  public minValue: number;
  public maxValue: number;

  constructor(minValue: number, maxValue: number) {
    this.minValue = minValue;
    this.maxValue = maxValue;
  }
}
