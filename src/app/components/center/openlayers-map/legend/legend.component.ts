/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Component, ElementRef, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { LegendSettings } from './legend-settings.class';

@Component({
  selector: 'app-legend',
  templateUrl: './legend.component.html',
  styleUrls: ['./legend.component.scss'],
})
export class LegendComponent {
  @Input()
  public settings$: Observable<LegendSettings>;

  public isLegendVisible = false;
  // Since it is not possible to animate auto <=> 0, we use max-width combined with custom values for the inner element for show/hide
  public width: number;
  public height: number;

  constructor(private el: ElementRef) {}

  public onToggleLegend(): void {
    this.width = this.isLegendVisible ? this.el.nativeElement.querySelector('.legend').offsetWidth : undefined;
    this.height = this.isLegendVisible ? this.el.nativeElement.querySelector('.legend').offsetHeight : undefined;
    this.isLegendVisible = !this.isLegendVisible;
  }
}
