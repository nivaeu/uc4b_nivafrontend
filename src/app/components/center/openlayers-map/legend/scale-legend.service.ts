import { Injectable } from '@angular/core';
import {
  AppliedSpecifiedAgriculturalApplication,
  ECropGeometry,
  ECropReport,
  QuantityGeometry,
} from '@app/components/left-side/interfaces/e-crop-report-message.class';
import { ScaleColorPerceptualService } from '@app/core/scale-color-perceptual/scale-color-perceptual.service';
import { LegendRange, LegendSettings, ScaleLegendItem, ScaleSummaryLine } from './legend-settings.class';

@Injectable()
export class ScaleLegendService {
  constructor(private scaleColorPerceptualService: ScaleColorPerceptualService) {}

  public getScaleLegendSettings(eCropReport: ECropReport): LegendSettings {
    const eCropGeometries = eCropReport.getGeometries();
    const unit = eCropReport.getUnit();
    const levels = 20;
    const legendRange = this.getLegendRange(eCropGeometries);
    const scaleLegendItems: ScaleLegendItem[] = this.getScaleLegendItems(legendRange, levels);
    const scaleSummaryLines: ScaleSummaryLine[] = this.getScaleSummaryLines(eCropGeometries, unit);

    return {
      items: scaleLegendItems,
      note: '',
      summaryLines: scaleSummaryLines,
      title: unit,
      unit: unit,
      visible: true,
    };
  }

  public getColorForValue(value: number, legend: LegendSettings) {
    return [...legend.items].find((item) => +item.text >= value)?.color;
  }

  private getScaleSummaryLines(eCropGeometries: ECropGeometry[], unit: string): ScaleSummaryLine[] {
    const appliedSpecifiedAgriculturalApplications = this.getAppliedSpecifiedAgriculturalApplications(eCropGeometries);
    const totalArea = appliedSpecifiedAgriculturalApplications.reduce((total, currentValue) => total + currentValue?.totalArea, 0);
    const totalProductAmount = appliedSpecifiedAgriculturalApplications.reduce(
      (total, currentValue) => total + currentValue?.totalProductAmount,
      0
    );
    let unitTotal: string;

    if (!totalArea || !totalProductAmount) {
      return [];
    }

    if (unit.indexOf('/') > 0) {
      unitTotal = unit.split('/')[0];
    } else if (unit.indexOf('kg') > -1) {
      unitTotal = 'kg';
    } else if (unit.indexOf('l') > -1) {
      unitTotal = 'l';
    } else {
      unitTotal = '';
    }

    return [
      {
        description: 'Total',
        quantity: totalProductAmount,
        unit: unitTotal,
      },
      {
        description: 'Avg',
        quantity: totalArea > 0 ? totalProductAmount / totalArea : totalArea,
        unit: unit,
      },
    ];
  }

  private getScaleLegendItems(legendRange: LegendRange, steps: number): ScaleLegendItem[] {
    const intervalLength = (legendRange.maxValue - legendRange.minValue) / steps;
    const values: number[] = [];
    for (let i = legendRange.maxValue; i >= legendRange.minValue; i = i - intervalLength) {
      values.push(i);
    }
    return values.reverse().map((v) => ({
      color: this.scaleColorPerceptualService.getViridisColor(1 - v / legendRange.maxValue) as string,
      text: v.toFixed(1),
    }));
  }

  private getLegendRange(eCropGeometries: ECropGeometry[]): LegendRange {
    const quantityGeometries: QuantityGeometry[] = eCropGeometries?.reduce((prev, curr) => [...prev, ...curr?.quantityGeometries], []);
    const quantities: number[] = quantityGeometries?.reduce((prev, curr) => [...prev, curr?.quantity], []);
    return new LegendRange(Math.min(...quantities), Math.max(...quantities));
  }

  private getAppliedSpecifiedAgriculturalApplications(eCropGeometries: ECropGeometry[]) {
    const appliedSpecifiedAgriculturalApplications: AppliedSpecifiedAgriculturalApplication[] = [];
    eCropGeometries?.forEach((eCropGeometry) =>
      eCropGeometry.specifiedCropPlot.grownFieldCrops?.forEach((grownFieldCrop) =>
        grownFieldCrop.applicableCropProductionAgriculturalProcess?.forEach((applicableCropProductionAgriculturalProcess) =>
          applicableCropProductionAgriculturalProcess.appliedSpecifiedAgriculturalApplications?.forEach(
            (appliedSpecifiedAgriculturalApplication) =>
              appliedSpecifiedAgriculturalApplications.push(appliedSpecifiedAgriculturalApplication)
          )
        )
      )
    );
    return appliedSpecifiedAgriculturalApplications;
  }
}
