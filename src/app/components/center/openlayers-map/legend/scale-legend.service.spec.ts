/* tslint:disable:no-unused-variable */

import { ScaleLegendService } from './scale-legend.service';
import { ScaleColorPerceptualService } from "@app/core/scale-color-perceptual/scale-color-perceptual.service";

describe('Service: ScaleLegend', () => {
  let service: ScaleLegendService
  let scaleColorPerceptualService: ScaleColorPerceptualService;
  beforeEach(() => {
    scaleColorPerceptualService = {} as ScaleColorPerceptualService;
    service = new ScaleLegendService(scaleColorPerceptualService)
  });

  it('should ...', () => {
    expect(service).toBeTruthy();
  });
});
