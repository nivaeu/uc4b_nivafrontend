/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { ECropReport } from '@app/components/left-side/interfaces/e-crop-report-message.class';
import { VraPrescriptionMap, VraPrescriptionMapCell } from '@app/components/left-side/interfaces/vra-prescription-map.class';
import { MapService } from '@app/core/map/map.service';
import { easeOut } from 'ol/easing';
import { Extent } from 'ol/extent';
import Feature from 'ol/Feature';
import WKT from 'ol/format/WKT';
import Geometry from 'ol/geom/Geometry';
import GeometryType from 'ol/geom/GeometryType';
import LineString from 'ol/geom/LineString';
import MultiPolygon from 'ol/geom/MultiPolygon';
import Point from 'ol/geom/Point';
import Polygon from 'ol/geom/Polygon';
import Select from 'ol/interaction/Select';
import Layer from 'ol/layer/Layer';
import VectorLayer from 'ol/layer/Vector';
import Map from 'ol/Map';
import MapBrowserEvent from 'ol/MapBrowserEvent';
import OlObject from 'ol/Object';
import Cluster from 'ol/source/Cluster';
import { default as Vector, default as VectorSource } from 'ol/source/Vector';
import CircleStyle from 'ol/style/Circle';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import { BehaviorSubject, fromEventPattern, Observable } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { LegendSettings } from './legend/legend-settings.class';
import { ScaleLegendService } from './legend/scale-legend.service';
import { OpenlayersMapService } from './openlayers-map.service';
import { LayerService } from './services/layer/layer.service';

@Component({
  selector: 'app-openlayers-map',
  templateUrl: './openlayers-map.component.html',
  styleUrls: ['./openlayers-map.component.scss'],
  providers: [OpenlayersMapService, ScaleLegendService],
})
export class OpenLayersMapComponent implements AfterViewInit, OnInit {
  private readonly isReadySubject = new BehaviorSubject(false);
  private map: Map;
  private layers: Layer[] = [];
  private selectInteraction: Select;

  @Input() public config?: any;
  @Input() public eCropReport$: Observable<ECropReport>;
  @Output() public readonly ready = new EventEmitter();
  @Output() public readonly moveEnd = new EventEmitter();
  public readonly isReady$ = this.isReadySubject.asObservable();
  public legendSettings$: BehaviorSubject<LegendSettings> = new BehaviorSubject<LegendSettings>(null);

  constructor(
    private mapService: MapService,
    private layerService: LayerService,
    private hostElement: ElementRef,
    private renderer: Renderer2,
    private openlayersMapService: OpenlayersMapService,
    private scaleLegendService: ScaleLegendService
  ) {}

  public ngOnInit() {
    this.eCropReport$.pipe(filter((eCropReport) => !!eCropReport)).subscribe((eCropReport) => {
      this.createLegendFromECrop(eCropReport);
      this.addOrUpdateECropToMap(eCropReport);
    });
  }

  public createLegendFromECrop(eCropReport: ECropReport) {
    const legendSettings = this.scaleLegendService.getScaleLegendSettings(eCropReport);
    this.legendSettings$.next(legendSettings);
  }

  public addOrUpdateECropToMap(eCropReport: ECropReport) {
    this.legendSettings$.pipe(first()).subscribe((legend) => {
      const polygonFeatures = [];
      const pointFeatures = [];
      const eCropGeometries = eCropReport.getGeometries();
      eCropGeometries.forEach((cropPlot) =>
        cropPlot.quantityGeometries.forEach((quantityGeometry) => {
          const geometry = quantityGeometry.geometry;
          const colorForValue = this.scaleLegendService.getColorForValue(quantityGeometry.quantity, legend);
          switch (geometry.type) {
            case GeometryType.POLYGON:
              const polygon = new Polygon(geometry.coordinates);
              const polygonFeature = new Feature();
              polygonFeature.setGeometry(this.transformGeometryToMap(polygon));
              polygonFeature.set('centerCoords', polygon.getInteriorPoint().getCoordinates());
              polygonFeature.set('stroke', '#ffffff');
              polygonFeature.set('fill', colorForValue);
              polygonFeatures.push(polygonFeature);
              break;
            case GeometryType.POINT:
              const point = new Point(geometry.coordinates);
              const pointFeature = new Feature(this.transformGeometryToMap(point));
              pointFeature.setGeometry(point);
              pointFeature.set('stroke', '#ffffff');
              pointFeature.set('fill', colorForValue);
              pointFeatures.push(pointFeature);
              break;
            default:
              throw new Error(`Only point and polygon geometry types are supported. Provided geometry type: ${geometry.type}`);
          }
        })
      );

      if (pointFeatures.length > 1) {
        const source = new VectorSource({ features: pointFeatures, wrapX: false });
        const layer = new VectorLayer({
          source: source,
          style: (feature: Feature) =>
            new Style({
              image: new CircleStyle({
                radius: 5,
                fill: new Fill({ color: feature.get('fill') }),
                stroke: new Stroke({ color: feature.get('fill'), width: 1 }),
              }),
            }),
        });
        this.layers.push(layer);
        this.map.addLayer(layer);
        this.zoomToLayer(layer);
      }

      if (polygonFeatures.length > 0) {
        const polygonLayer = this.addOrUpdateLayerToMap(
          {
            clustered: true,
            isVisible: true,
            layerId: 'fields',
            layerType: 1,
            zIndex: 3,
          },
          polygonFeatures
        );
        this.zoomToLayer(polygonLayer);
      }
    });
  }

  /**
   * Fits the provided map's view to the provided vra layer's prescription map features.
   * @param vraLayer The layer with the selected prescription maps
   */
  public zoomToLayer(vraLayer: Layer) {
    if (!vraLayer) {
      return;
    }

    const vraExtent = (vraLayer.getSource() as VectorSource).getExtent();

    if (vraExtent.some((ext) => !isFinite(ext))) {
      return;
    }

    this.slideMapToExtent(vraExtent);
  }

  public getVraLayerSettingsAndFeatures(selectedPrescriptionMaps: VraPrescriptionMap[], settings: any) {
    const vraCellsWithPolygonCoordinates = this.getVraCellsWithPolygonCoordinates(selectedPrescriptionMaps);
    const vraFeaturesAccumulated = this.getVraFeatures(vraCellsWithPolygonCoordinates);

    const vraLayerSetting = settings ? settings.layers[0] : undefined;

    return { vraLayerSetting, vraFeaturesAccumulated };
  }

  public getVraFeatures(vraCellsWithPolygonCoordinates: any[]) {
    return vraCellsWithPolygonCoordinates.map((cellWithPolygonCoordinates) => {
      const cell = cellWithPolygonCoordinates.cell;
      const feature = cellWithPolygonCoordinates.isMultiPolygon
        ? this.createMultiPolygonFeature('vra', cellWithPolygonCoordinates.polygonCoordinates)
        : this.createPolygonFeature('vra', cellWithPolygonCoordinates.polygonCoordinates);
      feature.setId(cell.id);
      feature.set('prescriptionMapId', cell.vraPrescriptionMapId);
      feature.set('quantityProduct', cell.quantityProduct);
      feature.set('quantityOrig', cell.quantityOrig);
      feature.set('quantity', cell.quantity);
      feature.set('color', cell.color);
      return feature;
    });
  }

  public createMultiPolygonFeature(layerId: string, coordinates: any): Feature {
    const feature = new Feature();
    feature.setGeometry(this.transformGeometryToMap(new MultiPolygon(coordinates)));
    feature.set('layerId', layerId);
    return feature;
  }

  public createPolygonFeature(layerId: string, coordinates: any): Feature {
    const feature = new Feature();
    feature.setGeometry(this.transformGeometryToMap(new Polygon(coordinates)));
    feature.set('layerId', layerId);
    return feature;
  }

  /**
   * Transforms geometry with correct projection
   * @param geometry geametry to transform
   */
  private transformGeometryToMap(geometry: Geometry): Geometry {
    return geometry.transform('EPSG:4326', 'EPSG:3857');
  }

  public getVraCellsWithPolygonCoordinates(vraPrescriptionMaps: VraPrescriptionMap[]) {
    return vraPrescriptionMaps
      .reduce((cells, prescriptionMap) => [...cells, ...prescriptionMap.cells], [] as VraPrescriptionMapCell[])
      .map((cell) => {
        const isMultiPolygon = cell.geometry.startsWith('MULTIPOLYGON');
        return {
          cell: cell,
          isMultiPolygon: isMultiPolygon,
          polygonCoordinates: isMultiPolygon
            ? this.getMultiPolygonCoordinatesFromWKTString(cell.geometry)
            : this.getPolygonCoordinatesFromWKTString(cell.geometry),
        };
      })
      .filter((cellWithPolygonCoordinates) => cellWithPolygonCoordinates.polygonCoordinates);
  }

  public getPolygonCoordinatesFromWKTString(wktString: string) {
    const coordinates = this.getCoordinatesFromWktString(wktString);
    return coordinates ? coordinates : null;
  }

  public getMultiPolygonCoordinatesFromWKTString(wktString: string) {
    const coordinates = this.getCoordinatesFromWktString(wktString);
    return coordinates ? coordinates : null;
  }

  public getCoordinatesFromWktString(geometryString: string) {
    const wkt = new WKT();
    const olGeometry = wkt.readGeometry(geometryString);

    switch (olGeometry.getType()) {
      case 'LineString':
        return (olGeometry as LineString).getCoordinates();
      case 'Point':
        return (olGeometry as Point).getCoordinates();
      case 'MultiPolygon':
        return (olGeometry as MultiPolygon).getCoordinates();
      default:
        return (olGeometry as Polygon).getCoordinates();
    }
  }

  public getGeometryTypeFromWktString(geometryString: string): GeometryType {
    const wkt = new WKT();
    const olGeometry = wkt.readGeometry(geometryString);

    switch (olGeometry.getType()) {
      case 'LineString':
        return GeometryType.LINE_STRING;
      case 'Point':
        return GeometryType.POINT;
      case 'MultiPolygon':
        return GeometryType.MULTI_POLYGON;
      default:
        return GeometryType.POLYGON;
    }
  }

  public ngAfterViewInit() {
    this.map = this.createMap(this.hostElement);
  }

  /**
   * Return map instance
   */
  public getMap(): Map {
    return this.map;
  }

  /**
   * Function to set the zoom level.
   * @param zoomLevel The target zoom level.
   */
  public zoom(zoomLevel: number = 7) {
    if (zoomLevel === undefined) {
      zoomLevel = 7;
    }

    this.map.getView().animate({
      zoom: zoomLevel,
      duration: 100,
    });
  }

  public zoomIncrementally(increment: number) {
    this.zoom(this.map.getView().getZoom() + increment);
  }

  /**
   * Adds a new layer corresponding to the given setting to map.
   * @param layerSetting settings describing new layer
   * @param features optinal initial features
   * @param customStyle optional custom style function
   */
  public addOrUpdateLayerToMap(layerSetting: any, features?: Feature[], customStyle?: Style): Layer {
    if (this.isLayerInMap(layerSetting.layerId)) {
      this.removeLayerFromMap(layerSetting.layerId);
    }

    const mapLayer = this.layerService.getLayer(layerSetting.layerType, features, customStyle);
    this.layers.push(mapLayer);
    this.map.addLayer(mapLayer);

    return mapLayer;
  }

  /**
   * Function determines whether or a layer with the provided id it present on map
   * @param id The id of the layer to check
   */
  public isLayerInMap(id: string): boolean {
    return this.layers.find((mapLayer: Layer) => mapLayer.get('id') === id) !== undefined;
  }

  /**
   * Function removed layer with given id from map, if it is present
   * @param id Id of layer to remove
   */
  public removeLayerFromMap(id: string): void {
    const index = this.layers.findIndex((mapLayer: Layer) => mapLayer.get('id') === id);

    if (index > -1) {
      this.map.removeLayer(this.layers[index]);
      this.layers.splice(index, 1);
    }
  }

  /**
   * Function returns layer with provided id. If layer is not found, function return undefined
   * @param id id of layer to return
   */
  public getLayerFromMap(id: string) {
    return this.layers.find((mapLayer: Layer) => mapLayer.get('id') === id);
  }

  /**
   * Add given features to layer width given id
   * @param layerId layer to add features onto
   * @param features features to add
   */
  public addFeaturesToLayer(layerId: string, features: Feature[]): void {
    const mapLayer: Layer = this.layers.find((l) => l.get('id') === layerId);

    if (mapLayer) {
      if (mapLayer.getSource() instanceof Cluster) {
        const layerSource = mapLayer.getSource() as Cluster;
        layerSource.getSource().addFeatures(features);
        layerSource.getSource().refresh();
      } else {
        const layerSource = mapLayer.getSource() as Vector;
        layerSource.addFeatures(features);
        layerSource.refresh();
      }
    }
  }

  public addPointerMoveInteraction() {
    return this.createEventObservable<MapBrowserEvent>('pointermove', this.map);
  }

  public deselectFeatures(): void {
    /**
     * HERE BE DRAGONS
     */
    if (!this.selectInteraction) {
      return;
    }

    try {
      // At first call this will apparently fail, for no good reason
      // This is the closest we have of an explanation:
      // https://github.com/openlayers/openlayers/issues/6183
      this.selectInteraction.getFeatures().clear();
    } catch (error) {
      // Second time around, it seems to succeed, but wont update the map
      this.selectInteraction.getFeatures().clear();
      // That is why we trigger an invisble zoom, to get the map to rerender
      this.forceRerender();
      throw error;
    }
  }

  public forceRerender() {
    this.zoomIncrementally(0.1);
    this.zoomIncrementally(-0.1);
  }

  public slideMapToExtent(e: Extent, padding?: number[]) {
    if (e) {
      this.map.getView().fit(e, {
        padding,
        easing: easeOut,
        duration: 500,
      });
    }
  }

  public onZoomIn() {
    this.zoomIncrementally(1);
  }
  public onZoomOut() {
    this.zoomIncrementally(-1);
  }

  private createMap(hostElement: ElementRef): Map {
    if (!hostElement || !hostElement.nativeElement) {
      return;
    }

    const $hostElement: HTMLElement = hostElement.nativeElement;
    const $mapElement = this.renderer.createElement('div');

    this.renderer.appendChild($hostElement, $mapElement);

    this.renderer.setStyle($mapElement, 'display', 'block');
    this.renderer.setStyle($mapElement, 'height', '100%');
    this.renderer.setStyle($mapElement, 'width', '100%');

    const osmLayer = this.layerService.getOSMLayer();
    this.layers.push(osmLayer);

    const olMap = this.openlayersMapService.createMap($mapElement);

    this.addOSMLayer(olMap);
    this.addBingMapsLayer(olMap);

    return olMap;
  }

  private addOSMLayer(olMap: Map) {
    const osmLayer = this.layerService.getOSMLayer();
    this.layers.push(osmLayer);
    olMap.addLayer(osmLayer);
  }

  private addBingMapsLayer(olMap: Map) {
    const bingMapsLayer = this.layerService.getBingMapsLayer();
    this.layers.push(bingMapsLayer);
    olMap.addLayer(bingMapsLayer);
  }

  private createEventObservable<TEvent>(event: string, mapSource?: OlObject) {
    return fromEventPattern<TEvent>(
      (handler: any) => mapSource.on(event, (e) => handler(e)),
      (handler: any) => mapSource.un(event, (e) => handler(e))
    );
  }
}
