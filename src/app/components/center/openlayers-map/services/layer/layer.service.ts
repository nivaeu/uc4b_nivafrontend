/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import Feature from 'ol/Feature';
import { default as Tile, default as TileLayer } from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import VectorImageLayer from 'ol/layer/VectorImage';
import BingMaps from 'ol/source/BingMaps';
import OSM from 'ol/source/OSM';
import Vector from 'ol/source/Vector';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import Text from 'ol/style/Text';

@Injectable({
  providedIn: 'root',
})
export class LayerService {
  constructor() {}

  public getLayer(layerType: number, features: Feature[], customStyle?) {
    switch (layerType) {
      case 1:
        return this.getVectorLayer(features, customStyle);
      case 2:
        return this.getVectorImageLayer(features, customStyle);
    }
  }

  public getOSMLayer() {
    return new TileLayer({
      source: new OSM(),
    });
  }

  public getBingMapsLayer() {
    const bingMapsLayer = new Tile({
      source: new BingMaps({
        key: environment.bingMaps.key,
        imagerySet: 'Aerial',
      }),
      visible: true,
    });

    bingMapsLayer.set('id', 'bing');

    return bingMapsLayer;
  }

  private getVectorImageLayer(features: Feature[], customStyle?) {
    return new VectorImageLayer({
      maxResolution: 200,
      minResolution: 0,
      zIndex: 2,
      source: new Vector({
        features: features || [],
      }),
      style: !!customStyle
        ? customStyle
        : (feature: Feature) => {
            const color = feature.get('color');

            return [
              new Style({
                fill: new Fill({
                  color: color,
                }),
              }),
            ];
          },
    });
  }

  private getVectorLayer(features: Feature[], customStyle?) {
    return new VectorLayer({
      visible: true,
      zIndex: 4,
      source: new Vector({
        features: features || [],
      }),
      style: !!customStyle
        ? customStyle
        : (feature: Feature) => {
            return [
              new Style({
                stroke: new Stroke({
                  color: feature.get('stroke'),
                  width: 2,
                }),
                text: new Text({
                  overflow: true,
                  text: feature.get('text'),
                  scale: 2,
                  stroke: new Stroke({
                    color: [0, 0, 0, 0.6],
                    width: 2.5,
                  }),
                  fill: new Fill({
                    color: [255, 255, 255, 1],
                  }),
                }),
                fill: new Fill({
                  color: feature.get('fill'),
                }),
              }),
            ];
          },
    });
  }
}
