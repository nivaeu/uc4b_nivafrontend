/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-map-zoom',
  templateUrl: './map-zoom.component.html',
  styleUrls: ['./map-zoom.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapZoomComponent {
  @Output() public zoomIn = new EventEmitter<void>();
  @Output() public zoomOut = new EventEmitter<void>();
  constructor() {}

  public onZoomIn() {
    this.zoomIn.emit();
  }

  public onZoomOut() {
    this.zoomOut.emit();
  }
}
