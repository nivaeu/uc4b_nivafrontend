/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MapZoomComponent } from './map-zoom.component';

describe('MapZoomComponent', () => {
  let component: MapZoomComponent;
  let fixture: ComponentFixture<MapZoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MapZoomComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapZoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
