/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { LocalStorageService } from '@app/core/local-storage/local-storage.service';

@Component({
  selector: 'app-harvest-year',
  templateUrl: './harvest-year.component.html',
  styleUrls: ['./harvest-year.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HarvestYearComponent {
  public harvestYear$ = this.localStorageService.getNivaHarvestYear$();

  constructor(private localStorageService: LocalStorageService) {}
}
