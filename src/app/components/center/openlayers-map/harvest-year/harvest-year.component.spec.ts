/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HarvestYearComponent } from './harvest-year.component';

describe('HarvestYearComponent', () => {
  let component: HarvestYearComponent;
  let fixture: ComponentFixture<HarvestYearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HarvestYearComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HarvestYearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
