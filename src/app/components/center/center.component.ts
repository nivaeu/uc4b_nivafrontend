/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import { ECropReport } from "@app/components/left-side/interfaces/e-crop-report-message.class";
import { NivaTask } from "@app/components/left-side/interfaces/niva-task.class";

@Component({
  selector: 'app-center',
  templateUrl: './center.component.html',
  styleUrls: ['./center.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CenterComponent {
  @Input() public eCropReport$: Observable<ECropReport>;
  @Input() public tasks$: Observable<NivaTask[]>;
}
