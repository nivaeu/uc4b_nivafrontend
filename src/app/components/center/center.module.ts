/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CenterComponent } from './center.component';
import { OpenLayersMapModule } from './openlayers-map/openlayers-map.module';

@NgModule({
  declarations: [CenterComponent],
  imports: [CommonModule, OpenLayersMapModule],
  exports: [CenterComponent],
})
export class CenterModule {}
