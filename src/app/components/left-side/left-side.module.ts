/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FarmIdModalComponent } from './farm-id-modal/farm-id-modal.component';
import { LeftSideComponent } from './left-side.component';
import { TaskTableComponent } from './task-table/task-table.component';

@NgModule({
  declarations: [LeftSideComponent, TaskTableComponent, FarmIdModalComponent],
  imports: [CommonModule],
  exports: [LeftSideComponent],
})
export class LeftSideModule {}
