/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
export enum VraOperationTypeGroupIds {
  MachineEvent = 1,
  Seeding = 2,
  Fertilizer = 3,
  PlantProtection = 4,
  Harvest = 5,
  Other = 6,
}
