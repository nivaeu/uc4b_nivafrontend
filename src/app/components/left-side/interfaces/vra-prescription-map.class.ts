/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { VraCustomLevelDTO, VraPrescriptionMapCellDTO, VraPrescriptionMapDTO } from './vra-prescription-map.interface';

export class VraPrescriptionMapCell {
  public id: number;
  public quantityOrig: number;
  public quantity: number;
  public colorOrig: string;
  public color: string;
  public geometry: string;
  public customLevel: boolean;
  public quantityProduct: number;

  constructor(dto: VraPrescriptionMapCellDTO) {
    this.id = dto.Id;
    this.quantityOrig = dto.QuantityOrig;
    this.quantity = dto.Quantity;
    this.colorOrig = dto.ColorOrig;
    this.color = dto.Color;
    this.geometry = dto.Geometry;
    this.customLevel = dto.CustomLevel;
    this.quantityProduct = dto.QuantityProduct;
  }
}

export class VraCustomLevel {
  public quantity: number;
  public color: string;

  constructor(dto: VraCustomLevelDTO) {
    this.quantity = dto.Quantity;
    this.color = dto.Color;
  }
}

export class VraPrescriptionMap {
  public id: number;
  public area: number;
  public farmId: number;
  public featureId: number;
  public operationId: number;
  public harvestYear: number;
  public ready: boolean;
  public totalQuantity: number;
  public totalProductQuantity: number;
  public cells: VraPrescriptionMapCell[];
  public customLevels: VraCustomLevel[];

  constructor(dto: VraPrescriptionMapDTO) {
    this.id = dto.Id;
    this.area = dto.Area;
    this.farmId = dto.FarmId;
    this.featureId = dto.FeatureId;
    this.operationId = dto.OperationId;
    this.harvestYear = dto.HarvestYear;
    this.ready = dto.Ready;
    this.totalQuantity = dto.TotalQuantity;
    this.totalProductQuantity = dto.TotalProductQuantity;
    this.cells = dto.Cells.map((cell) => new VraPrescriptionMapCell(cell));
    this.customLevels = dto.CustomLevels.map((customLevel) => new VraCustomLevel(customLevel));
  }
}
