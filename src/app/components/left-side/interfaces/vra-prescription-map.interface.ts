/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
export interface VraPrescriptionMapCellDTO {
  Id: number;
  QuantityOrig: number;
  Quantity: number;
  ColorOrig: string;
  Color: string;
  Geometry: string;
  CustomLevel: boolean;
  QuantityProduct: number;
}

export interface VraCustomLevelDTO {
  Quantity: number;
  Color: string;
}

export interface VraPrescriptionMapDTO {
  Id: number;
  Area: number;
  FarmId: number;
  FeatureId: number;
  OperationId: number;
  HarvestYear: number;
  Ready: boolean;
  TotalQuantity: number;
  TotalProductQuantity: number;
  Cells: VraPrescriptionMapCellDTO[];
  CustomLevels: VraCustomLevelDTO[];
}
