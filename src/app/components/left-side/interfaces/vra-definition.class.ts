/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { LegendColorValueDTO, VraDefinitionDTO, VraDefinitionInfoDTO } from './vra-definition.interface';
import { VraPrescriptionMap } from './vra-prescription-map.class';

export class LegendColorValue {
  public color: string;
  public value: number;

  constructor(dto: LegendColorValueDTO) {
    this.color = dto.Color;
    this.value = dto.Value;
  }
}

export class VraDefinitionInfo {
  public unitText: string;

  constructor(dto: VraDefinitionInfoDTO) {
    this.unitText = dto.UnitText;
  }
}

export class VraDefinition {
  public info: VraDefinitionInfo;
  public vraPrescriptionMaps: VraPrescriptionMap[];
  public legend: LegendColorValue[];
  public avgQuantity: number;
  public totalQuantity: number;
  public area: number;

  constructor(dto: VraDefinitionDTO) {
    this.info = new VraDefinitionInfo(dto.Info);
    this.vraPrescriptionMaps = dto.VraPrescriptionMaps.map((vraPrescriptionMapDTO) => new VraPrescriptionMap(vraPrescriptionMapDTO));
    this.legend = dto.Legend.map((legendDTO) => new LegendColorValue(legendDTO));
    this.avgQuantity = dto.AvgQuantity;
    this.totalQuantity = dto.TotalQuantity;
    this.area = dto.Area;
  }
}
