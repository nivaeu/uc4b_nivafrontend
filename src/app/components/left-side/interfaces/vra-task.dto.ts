/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { HttpErrorResponse } from '@angular/common/http';
import { VraOperationTypeGroupIds } from './vra-operation-type-group-ids.enum';

export interface VraOperationTypeGroupDTO {
  Id: VraOperationTypeGroupIds;
  Name: string;
  Tasks: VraTaskDto[];
}

export interface VraTaskDto {
  HashCode: string;
  Date: string;
  CropName: string;
  Fields: VraTaskFieldDto[];
}

export interface VraTaskFieldDto {
  FeatureId: number;
  FieldId: number;
  FieldName: string;
  FieldNumber: string;
  Geometry: string;
  OperationLines: VraOperationLineDto[];
  VraPrescriptionMapId: number;
}

export interface VraOperationLineDto {
  Id: number;
  OperationId: number;
  Unit: string;
  ProduceName: string;
  ProduceNormNumber: number;
  Quantity: number;
  TotalQuantity: number;
  TotalUnit: string;
}

export interface VraValidationDTO {
  ErrorMessage: string;
  ErrorCode: number;
  SpecifiedEntityName: string;
  SpecifiedEntity: string;
}

export interface ReceiptAppliedDTO {
  AreaAppliedTotal: number;
  AverageApplicationRate: number;
  ApplicationDate: Date;
  MaxApplicationRate: number;
  MinApplicationRate: number;
}

export interface ReceiptCropPlotDTO {
  Year: number;
  FieldNumber: string;
  LegislationCrop: string[];
  CropConstituents: string[];
  AppliedList: ReceiptAppliedDTO[];
  Produce: string[];
  DescriptiveStatistics: DescriptiveStatisticsDTO;
}

export interface ECropReceiptDTO {
  Sender: string;
  Recipient: string;
  Purpose: string;
  CropPlot: ReceiptCropPlotDTO[];
  error?: HttpErrorResponse;
}

export interface ECropMessageDTO {
  Element: string;
  Severity: string;
  MessageCode: number;
  Message: string;
}

export interface ECROPReturnMessageDTO {
  ReturnCode: number;
  ReturnMessage: string;
  MessageCount: number;
  TimeStamp: Date;
  Messages: ECropMessageDTO[];
}

export interface DescriptiveStatisticsDTO {
  Mean: NivaStatisticsDTO;
  CoefficientVariation: NivaStatisticsDTO;
  Minimum: NivaStatisticsDTO;
  Maximum: NivaStatisticsDTO;
}

export interface NivaStatisticsDTO {
  Value: number;
  Unit: string;
}
