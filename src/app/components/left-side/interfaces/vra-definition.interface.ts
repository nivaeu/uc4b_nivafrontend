/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { VraPrescriptionMapDTO } from './vra-prescription-map.interface';

export interface LegendColorValueDTO {
  Color: string;
  Value: number;
}

export interface VraDefinitionInfoDTO {
  UnitText: string;
}

export interface VraDefinitionDTO {
  Info: VraDefinitionInfoDTO;
  VraPrescriptionMaps: VraPrescriptionMapDTO[];
  Legend: LegendColorValueDTO[];
  AvgQuantity: number;
  TotalQuantity: number;
  Area: number;
}
