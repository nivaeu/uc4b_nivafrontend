import {
  AgriculturalProducerPartyDto,
  AgriculturalProductionUnitDto,
  ApplicableCropProductionAgriculturalProcessDto,
  AppliedReferencedLocationDto,
  AppliedSpecifiedAgriculturalApplicationDto,
  AppliedSpecifiedAgriculturalApplicationRateDto,
  CropReportDocumentDto,
  ECROPReportDto,
  ECROPReportMessageDto,
  GeometryDto,
  GrownFieldCropDto,
  PhysicalSpecifiedGeographicalFeatureDto,
  SownCropSpeciesVarietyDto,
  SpecifiedBotanicalCropDto,
  SpecifiedCropPlotDto,
  SpecifiedFieldCropMixtureConstituentDto,
  SpecifiedPartyDto,
  SpecifiedProductBatchDto
} from './e-crop-report-message.dto';

export class Geometry {
  public type: string;
  public coordinates: number[];

  constructor(dto: GeometryDto) {
    this.type = dto.type;
    this.coordinates = dto.coordinates;
  }
}

export class SpecifiedProductBatch {
  public id: string;
  public typeCode: string;
  public productName: string;
  public sizeMeasure: number;
  public unitQuantity: number;
  public weightMeasure: number;
  public appliedTreatment: string;

  constructor(dto: SpecifiedProductBatchDto) {
    this.id = dto.ID;
    this.typeCode = dto.TypeCode;
    this.productName = dto.ProductName;
    this.sizeMeasure = dto.SizeMeasure;
    this.unitQuantity = dto.UnitQuantity;
    this.weightMeasure = dto.WeightMeasure;
    this.appliedTreatment = dto.AppliedTreatment;
  }
}

export class SownCropSpeciesVariety {
  public typeCode: string;
  public description: string;

  constructor(dto: SownCropSpeciesVarietyDto) {
    this.typeCode = dto.TypeCode;
    this.description = dto.Description;
  }
}

export class SpecifiedParty {
  public id: string;
  public name: string;
  public countryId: string;

  constructor(specifiedPartyDto: SpecifiedPartyDto) {
    this.id = specifiedPartyDto.ID;
    this.name = specifiedPartyDto.Name;
    this.countryId = specifiedPartyDto.CountryID;
  }
}

export class PhysicalSpecifiedGeographicalFeature {
  public type: string;
  public geometry: Geometry;

  constructor(dto: PhysicalSpecifiedGeographicalFeatureDto) {
    this.type = dto.type;
    this.geometry = dto.geometry;
  }
}

export class AppliedReferencedLocation {
  public id: string;
  public name: string;
  public typeCode: string;
  public description: string;
  public referenceTypeCode: string;
  public physicalSpecifiedGeographicalFeature: PhysicalSpecifiedGeographicalFeature;

  constructor(dto: AppliedReferencedLocationDto) {
    this.id = dto.ID;
    this.name = dto.Name;
    this.typeCode = dto.TypeCode;
    this.description = dto.Description;
    this.referenceTypeCode = dto.ReferenceTypeCode;
    this.physicalSpecifiedGeographicalFeature = dto.PhysicalSpecifiedGeographicalFeature;
  }
}

export class AppliedSpecifiedAgriculturalApplicationRate {
  public appliedQuantity: number;
  public appliedQuantityUnit: string;
  public appliedReferencedLocations: AppliedReferencedLocation[];

  constructor(dto: AppliedSpecifiedAgriculturalApplicationRateDto) {
    this.appliedQuantity = dto.AppliedQuantity;
    this.appliedQuantityUnit = dto.AppliedQuantityUnit;
    this.appliedReferencedLocations = dto.AppliedReferencedLocation.map(
      (appliedReferencedLocation) => new AppliedReferencedLocation(appliedReferencedLocation)
    );
  }
}

export class AppliedSpecifiedAgriculturalApplication {
  public id: string;
  public specifiedProductBatchs: SpecifiedProductBatch[];
  public appliedSpecifiedAgriculturalApplicationRates: AppliedSpecifiedAgriculturalApplicationRate[];
  public totalArea: number;
  public totalProductAmount: number;

  constructor(dto: AppliedSpecifiedAgriculturalApplicationDto) {
    this.id = dto.ID;
    this.specifiedProductBatchs = dto.SpecifiedProductBatch.map(
      (specifiedProductBatchDto) => new SpecifiedProductBatch(specifiedProductBatchDto)
    );
    this.appliedSpecifiedAgriculturalApplicationRates = dto.AppliedSpecifiedAgriculturalApplicationRate.map(
      (appliedSpecifiedAgriculturalApplicationRate) =>
        new AppliedSpecifiedAgriculturalApplicationRate(appliedSpecifiedAgriculturalApplicationRate)
    );
    this.totalArea = dto.TotalArea;
    this.totalProductAmount = dto.TotalProductAmount;
  }
}

export class ApplicableCropProductionAgriculturalProcess {
  public typeCode: string;
  public actualEndDateTime: string;
  public actualStartDateTime: string;
  public subordinateTypeCode: string;
  public appliedSpecifiedAgriculturalApplications: AppliedSpecifiedAgriculturalApplication[];

  constructor(dto: ApplicableCropProductionAgriculturalProcessDto) {
    this.typeCode = dto.TypeCode;
    this.actualEndDateTime = dto.ActualEndDateTime;
    this.actualStartDateTime = dto.ActualStartDateTime;
    this.subordinateTypeCode = dto.SubordinateTypeCode;
    this.appliedSpecifiedAgriculturalApplications = dto.AppliedSpecifiedAgriculturalApplication.map(
      (appliedSpecifiedAgriculturalApplication) => new AppliedSpecifiedAgriculturalApplication(appliedSpecifiedAgriculturalApplication)
    );
  }
}

export class SpecifiedBotanicalCrop {
  public purposeCode: string;
  public botanicalName: string;
  public botanicalGenusCode: string;
  public botanicalSpeciesCode: string;
  public sownCropSpeciesVariety: SownCropSpeciesVariety[];
  public botanicalIdentificationId: string;

  constructor(dto: SpecifiedBotanicalCropDto) {
    this.purposeCode = dto.PurposeCode;
    this.botanicalName = dto.BotanicalName;
    this.botanicalGenusCode = dto.BotanicalGenusCode;
    this.botanicalSpeciesCode = dto.BotanicalSpeciesCode;
    this.sownCropSpeciesVariety = dto.SownCropSpeciesVariety.map(
      (sownCropSpeciesVarietyDto) => new SownCropSpeciesVariety(sownCropSpeciesVarietyDto)
    );
    this.botanicalIdentificationId = dto.BotanicalIdentificationID;
  }
}

export class SpecifiedFieldCropMixtureConstituent {
  public cropProportionPercent: number;
  public specifiedBotanicalCrop: SpecifiedBotanicalCrop;

  constructor(dto: SpecifiedFieldCropMixtureConstituentDto) {
    this.cropProportionPercent = dto.CropProportionPercent;
    this.specifiedBotanicalCrop = new SpecifiedBotanicalCrop(dto.SpecifiedBotanicalCrop);
  }
}

export class GrownFieldCrop {
  public classificationCode: string;
  public specifiedFieldCropMixtureConstituents: SpecifiedFieldCropMixtureConstituent[];
  public applicableCropProductionAgriculturalProcess: ApplicableCropProductionAgriculturalProcess[];

  constructor(dto: GrownFieldCropDto) {
    this.classificationCode = dto.ClassificationCode;
    this.specifiedFieldCropMixtureConstituents = dto.SpecifiedFieldCropMixtureConstituent.map(
      (specifiedFieldCropMixtureConstituent) => new SpecifiedFieldCropMixtureConstituent(specifiedFieldCropMixtureConstituent)
    );
    this.applicableCropProductionAgriculturalProcess = dto.ApplicableCropProductionAgriculturalProcess.map(
      (process) => new ApplicableCropProductionAgriculturalProcess(process)
    );
  }
}

export class SpecifiedCropPlot {
  public id: string;
  public areaMeasure: number;
  public endDateTime: string;
  public startDateTime: string;
  public grownFieldCrops: GrownFieldCrop[];
  public specifiedReferencedLocation: AppliedReferencedLocation;

  constructor(dto: SpecifiedCropPlotDto) {
    this.id = dto.ID;
    this.areaMeasure = dto.AreaMeasure;
    this.endDateTime = dto.EndDateTime;
    this.startDateTime = dto.StartDateTime;
    this.grownFieldCrops = dto.GrownFieldCrop.map((grownFieldCropDto) => new GrownFieldCrop(grownFieldCropDto));
    this.specifiedReferencedLocation = new AppliedReferencedLocation(dto.SpecifiedReferencedLocation);
  }
}

export class AgriculturalProductionUnit {
  public id: string;
  public name: string;
  public typeCode: string;
  public specifiedCropPlots: SpecifiedCropPlot[];

  constructor(dto: AgriculturalProductionUnitDto) {
    this.id = dto.ID;
    this.name = dto.Name;
    this.typeCode = dto.TypeCode;
    this.specifiedCropPlots = dto.SpecifiedCropPlot.map((specifiedCropPlotDto) => new SpecifiedCropPlot(specifiedCropPlotDto));
  }
}

export class AgriculturalProducerParty {
  public id: string;
  public name: string;
  public description: string;
  public classificationCode: string;
  public agriculturalProductionUnit: AgriculturalProductionUnit;

  constructor(dto: AgriculturalProducerPartyDto) {
    this.id = dto.ID;
    this.name = dto.Name;
    this.description = dto.Description;
    this.classificationCode = dto.ClassificationCode;
    this.agriculturalProductionUnit = new AgriculturalProductionUnit(dto.AgriculturalProductionUnit);
  }
}

export class CropReportDocument {
  public id: string;
  public typeCode: string;
  public sequenceId: string;
  public statusCode: string;
  public description: string;
  public information: string;
  public purposeCode: string;
  public copyIndicator: boolean;
  public issueDateTime: string;
  public lineCountNumeric: number;
  public reportCountNumeric: number;
  public senderSpecifiedParty: SpecifiedParty;
  public recipientSpecifiedParty: SpecifiedParty;
  public controlRequirementIndicator: boolean;

  constructor(dto: CropReportDocumentDto) {
    this.id = dto.ID;
    this.typeCode = dto.SequenceID;
    this.sequenceId = dto.SequenceID;
    this.statusCode = dto.StatusCode;
    this.description = dto.Description;
    this.information = dto.Information;
    this.purposeCode = dto.PurposeCode;
    this.copyIndicator = dto.CopyIndicator;
    this.issueDateTime = dto.IssueDateTime;
    this.lineCountNumeric = dto.LineCountNumeric;
    this.reportCountNumeric = dto.ReportCountNumeric;
    this.senderSpecifiedParty = new SpecifiedParty(dto.SenderSpecifiedParty);
    this.recipientSpecifiedParty = new SpecifiedParty(dto.RecipientSpecifiedParty);
    this.controlRequirementIndicator = dto.ControlRequirementIndicator;
  }
}

export class ECropReportMessage {
  public cropReportDocument: CropReportDocument;
  public agriculturalProducerParty: AgriculturalProducerParty;

  constructor(dto: ECROPReportMessageDto) {
    this.cropReportDocument = new CropReportDocument(dto.CropReportDocument);
    this.agriculturalProducerParty = new AgriculturalProducerParty(dto.AgriculturalProducerParty);
  }
}

export class QuantityGeometry {
  public quantity: number;
  public geometry: Geometry;

  constructor(quantity: number, geometry: Geometry) {
    this.quantity = quantity;
    this.geometry = geometry;
  }
}

export class ECropGeometry {
  public specifiedCropPlot: SpecifiedCropPlot;
  public quantityGeometries: QuantityGeometry[];

  constructor(specifiedCropPlot, quantityGeometries) {
    this.specifiedCropPlot = specifiedCropPlot;
    this.quantityGeometries = quantityGeometries;
  }
}

export class ECropReport {
  public eCropReportMessage: ECropReportMessage;

  constructor(dto: ECROPReportDto) {
    this.eCropReportMessage = new ECropReportMessage(dto.ECROPReportMessage);
  }

  public getGeometries(): ECropGeometry[] {
    const specifiedCropPlotsWithGeometries: ECropGeometry[] = [];
    this.eCropReportMessage.agriculturalProducerParty.agriculturalProductionUnit.specifiedCropPlots.forEach((specifiedCropPlot) => {
      specifiedCropPlot.grownFieldCrops.forEach((grownFieldCrop) => {
        const eCropGeometries: QuantityGeometry[] = [];
        grownFieldCrop.applicableCropProductionAgriculturalProcess.forEach((process) => {
          process.appliedSpecifiedAgriculturalApplications.forEach((appliedSpecifiedAgriculturalApplication) => {
            appliedSpecifiedAgriculturalApplication.appliedSpecifiedAgriculturalApplicationRates.forEach(
              (appliedSpecifiedAgriculturalApplicationRate) => {
                const quantity = appliedSpecifiedAgriculturalApplicationRate.appliedQuantity;
                const geometries = appliedSpecifiedAgriculturalApplicationRate.appliedReferencedLocations.map(
                  (appliedReferencedLocation) =>
                    new QuantityGeometry(quantity, appliedReferencedLocation.physicalSpecifiedGeographicalFeature.geometry)
                );
                eCropGeometries.push(...geometries);
              }
            );
          });
        });
        specifiedCropPlotsWithGeometries.push(new ECropGeometry(specifiedCropPlot, eCropGeometries));
      });
    });
    return specifiedCropPlotsWithGeometries;
  }

  /**
   * Returns the unit of the ECrop message.
   * This will only work if all
   */
  public getUnit() {
    const firstUnit =
      this.eCropReportMessage.agriculturalProducerParty.agriculturalProductionUnit.specifiedCropPlots[0].grownFieldCrops[0]
        .applicableCropProductionAgriculturalProcess[0].appliedSpecifiedAgriculturalApplications[0]
        .appliedSpecifiedAgriculturalApplicationRates[0].appliedQuantityUnit;
    const allSameUnit = this.eCropReportMessage.agriculturalProducerParty.agriculturalProductionUnit.specifiedCropPlots.every(
      (specifiedCropPlot) =>
        specifiedCropPlot.grownFieldCrops.every((grownFieldCrop) =>
          grownFieldCrop.applicableCropProductionAgriculturalProcess.every((applicableCropProductionAgriculturalProcess) =>
            applicableCropProductionAgriculturalProcess.appliedSpecifiedAgriculturalApplications.every(
              (appliedSpecifiedAgriculturalApplication) =>
                appliedSpecifiedAgriculturalApplication.appliedSpecifiedAgriculturalApplicationRates.every(
                  (appliedSpecifiedAgriculturalApplicationRate) =>
                    appliedSpecifiedAgriculturalApplicationRate.appliedQuantityUnit === firstUnit
                )
            )
          )
        )
    );
    if (!allSameUnit) {
      return 'mixed units';
    }
    return firstUnit;
  }
}
