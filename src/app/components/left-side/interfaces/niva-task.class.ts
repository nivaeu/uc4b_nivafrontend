/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { HttpErrorResponse } from '@angular/common/http';
import {
  DescriptiveStatisticsDTO,
  ECropMessageDTO,
  ECropReceiptDTO,
  ECROPReturnMessageDTO,
  NivaStatisticsDTO,
  ReceiptAppliedDTO,
  ReceiptCropPlotDTO,
  VraValidationDTO,
} from './vra-task.dto';

export class NivaValidation {
  public eErrorMessage: string;
  public eErrorCode: number;
  public specifiedEntityName: string;
  public specifiedEntity: string;

  constructor(dto: VraValidationDTO) {
    this.eErrorMessage = dto.ErrorMessage;
    this.eErrorCode = dto.ErrorCode;
    this.specifiedEntityName = dto.SpecifiedEntityName;
    this.specifiedEntity = dto.SpecifiedEntity;
  }
}

export class NivaReceiptApplied {
  public areaAppliedTotal: number;
  public averageApplicationRate: number;
  public applicationDate: Date;
  public maxApplicationRate: number;
  public minApplicationRate: number;

  public constructor(dto: ReceiptAppliedDTO) {
    this.areaAppliedTotal = isNaN(dto.AreaAppliedTotal) ? 0 : dto.AreaAppliedTotal;
    this.averageApplicationRate = isNaN(dto.AverageApplicationRate) ? 0 : dto.AverageApplicationRate;
    this.applicationDate = dto.ApplicationDate;
    this.maxApplicationRate = isNaN(dto.MaxApplicationRate) ? 0 : dto.MaxApplicationRate;
    this.minApplicationRate = isNaN(dto.MinApplicationRate) ? 0 : dto.MinApplicationRate;
  }
}

export class NivaStatistics {
  public value: number;
  public unit: string;

  constructor(dto: NivaStatisticsDTO) {
    this.value = dto.Value;
    this.unit = dto.Unit;
  }
}

export class NivaDescriptiveStatistics {
  public mean: NivaStatistics;
  public coefficientVariation: NivaStatistics;
  public minimum: NivaStatistics;
  public maximum: NivaStatistics;

  constructor(dto: DescriptiveStatisticsDTO) {
    if (dto) {
      this.mean = new NivaStatistics(dto.Mean);
      this.coefficientVariation = new NivaStatistics(dto.CoefficientVariation);
      this.maximum = new NivaStatistics(dto.Maximum);
      this.minimum = new NivaStatistics(dto.Minimum);
    }
  }
}

export class NivaReceiptCropPlot {
  public year: number;
  public fieldNumber: string;
  public legislationCrop: string[];
  public cropConstituents: string[];
  public appliedList: NivaReceiptApplied[];
  public produce: string[];
  public descriptiveStatistics: NivaDescriptiveStatistics;

  public constructor(dto: ReceiptCropPlotDTO) {
    this.year = dto.Year;
    this.fieldNumber = dto.FieldNumber;
    this.legislationCrop = dto.LegislationCrop;
    this.cropConstituents = dto.CropConstituents;
    this.appliedList = dto.AppliedList.map((oneAppliedList) => new NivaReceiptApplied(oneAppliedList));
    this.produce = dto.Produce;
    this.descriptiveStatistics = new NivaDescriptiveStatistics(dto.DescriptiveStatistics);
  }
}

export class NivaECropReceipt {
  public sender: string;
  public recipient: string;
  public purpose: string;
  public cropPlot: NivaReceiptCropPlot[];
  public error?: HttpErrorResponse;

  public constructor(dto: ECropReceiptDTO) {
    this.sender = dto.Sender;
    this.recipient = dto.Recipient;
    this.purpose = dto.Purpose;
    this.cropPlot = dto.CropPlot.map((oneCropPlot) => new NivaReceiptCropPlot(oneCropPlot));
    this.error = dto.error;
  }
}

export interface NivaTaskDTO {
  Crop: string;
  Date: Date;
  Id: number;
  Product: string;
}

export class NivaTask {
  public id: number;
  public product: string;
  public crop: string;
  public date: Date;
  public validations?: NivaValidation[];

  constructor(dto: NivaTaskDTO) {
    this.id = dto.Id;
    this.product = dto.Product;
    this.crop = dto.Crop;
    this.date = dto.Date;
    this.validations = [];
  }
}

export class NivaECropMessage {
  public element: string;
  public severity: string;
  public messageCode: number;
  public message: string;

  constructor(dto: ECropMessageDTO) {
    this.element = dto.Element;
    this.severity = dto.Severity;
    this.messageCode = dto.MessageCode;
    this.message = dto.Message;
  }
}

export class NivaEcropReturnMessage {
  public returnCode: number;
  public returnMessage: string;
  public messageCount: number;
  public timeStamp: Date;
  public messages: NivaECropMessage[];

  constructor(dto: ECROPReturnMessageDTO) {
    this.returnCode = dto.ReturnCode;
    this.returnMessage = dto.ReturnMessage;
    this.messageCount = dto.MessageCount;
    this.timeStamp = dto.TimeStamp;
    this.messages = dto.Messages.map((eCropMessageDTO) => new NivaECropMessage(eCropMessageDTO));
  }
}
