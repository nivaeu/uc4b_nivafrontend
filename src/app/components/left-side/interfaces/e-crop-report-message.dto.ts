export interface GeometryDto {
  type: string;
  coordinates: number[];
}

export interface SpecifiedProductBatchDto {
  ID: string;
  TypeCode: string;
  ProductName: string;
  SizeMeasure: number;
  UnitQuantity: number;
  WeightMeasure: number;
  AppliedTreatment: string;
}

export interface SownCropSpeciesVarietyDto {
  TypeCode: string;
  Description: string;
}

export interface SpecifiedPartyDto {
  ID: string;
  Name: string;
  CountryID: string;
}

export interface PhysicalSpecifiedGeographicalFeatureDto {
  type: string;
  geometry: GeometryDto;
}

export interface AppliedReferencedLocationDto {
  ID: string;
  Name: string;
  TypeCode: string;
  Description: string;
  ReferenceTypeCode: string;
  PhysicalSpecifiedGeographicalFeature: PhysicalSpecifiedGeographicalFeatureDto;
}

export interface AppliedSpecifiedAgriculturalApplicationRateDto {
  AppliedQuantity: number;
  AppliedQuantityUnit: string;
  AppliedReferencedLocation: AppliedReferencedLocationDto[];
}

export interface AppliedSpecifiedAgriculturalApplicationDto {
  ID: string;
  SpecifiedProductBatch: SpecifiedProductBatchDto[];
  TotalArea: number;
  TotalProductAmount: number;
  AppliedSpecifiedAgriculturalApplicationRate: AppliedSpecifiedAgriculturalApplicationRateDto[];
}

export interface ApplicableCropProductionAgriculturalProcessDto {
  TypeCode: string;
  ActualEndDateTime: string;
  ActualStartDateTime: string;
  SubordinateTypeCode: string;
  AppliedSpecifiedAgriculturalApplication: AppliedSpecifiedAgriculturalApplicationDto[];
}

export interface SpecifiedBotanicalCropDto {
  PurposeCode: string;
  BotanicalName: string;
  BotanicalGenusCode: string;
  BotanicalSpeciesCode: string;
  SownCropSpeciesVariety: SownCropSpeciesVarietyDto[];
  BotanicalIdentificationID: string;
}

export interface SpecifiedFieldCropMixtureConstituentDto {
  CropProportionPercent: number;
  SpecifiedBotanicalCrop: SpecifiedBotanicalCropDto;
}

export interface GrownFieldCropDto {
  ClassificationCode: string;
  ClassificationName: string;
  SpecifiedFieldCropMixtureConstituent: SpecifiedFieldCropMixtureConstituentDto[];
  ApplicableCropProductionAgriculturalProcess: ApplicableCropProductionAgriculturalProcessDto[];
}

export interface SpecifiedCropPlotDto {
  ID: string;
  AreaMeasure: number;
  EndDateTime: string;
  StartDateTime: string;
  GrownFieldCrop: GrownFieldCropDto[];
  SpecifiedReferencedLocation: AppliedReferencedLocationDto;
}

export interface AgriculturalProductionUnitDto {
  ID: string;
  Name: string;
  TypeCode: string;
  SpecifiedCropPlot: SpecifiedCropPlotDto[];
}

export interface AgriculturalProducerPartyDto {
  ID: string;
  Name: string;
  Description: string;
  ClassificationCode: string;
  AgriculturalProductionUnit: AgriculturalProductionUnitDto;
}

export interface CropReportDocumentDto {
  ID: string;
  TypeCode: string;
  SequenceID: string;
  StatusCode: string;
  Description: string;
  Information: string;
  PurposeCode: string;
  CopyIndicator: boolean;
  IssueDateTime: string;
  LineCountNumeric: number;
  ReportCountNumeric: number;
  SenderSpecifiedParty: SpecifiedPartyDto;
  RecipientSpecifiedParty: SpecifiedPartyDto;
  ControlRequirementIndicator: boolean;
}

export interface ECROPReportMessageDto {
  CropReportDocument: CropReportDocumentDto;
  AgriculturalProducerParty: AgriculturalProducerPartyDto;
}

export interface ECROPReportDto {
  ECROPReportMessage: ECROPReportMessageDto;
}
