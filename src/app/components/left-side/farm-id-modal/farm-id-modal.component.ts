/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { LocalStorageService } from '@app/core/local-storage/local-storage.service';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-farm-id-modal',
  templateUrl: './farm-id-modal.component.html',
  styleUrls: ['./farm-id-modal.component.scss'],
})
export class FarmIdModalComponent implements OnInit {
  @ViewChild('myModal', { static: false }) public modal: ElementRef;
  public farmId$: Observable<number>;
  public harvestYear$: Observable<number>;

  public constructor(private localStorageService: LocalStorageService) {}

  public ngOnInit(): void {
    this.farmId$ = this.localStorageService.getNivafarmId$();
    this.harvestYear$ = this.localStorageService.getNivaHarvestYear$();
  }

  public open() {
    this.modal.nativeElement.style.display = 'block';
  }

  public close() {
    this.modal.nativeElement.style.display = 'none';
  }

  public changeFarmIdAndHarvestYear(farmId: string, harvestYear: string) {
    this.localStorageService.farmId = Number(farmId);
    this.localStorageService.harvestYear = Number(harvestYear);
    this.close();
  }
}
