/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LocalStorageService } from '@app/core/local-storage/local-storage.service';
import { Mock, SpyHelper } from '@app/helpers/test/spy-helper';
import { of } from 'rxjs';
import { FarmIdModalComponent } from './farm-id-modal.component';

describe('FarmIdModalComponent', () => {
  let component: FarmIdModalComponent;
  let fixture: ComponentFixture<FarmIdModalComponent>;
  let farmIdServiceStub: Mock<LocalStorageService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FarmIdModalComponent],
      providers: [SpyHelper.provideMagicalMock(LocalStorageService)],
    }).compileComponents();

    farmIdServiceStub = TestBed.get(LocalStorageService);
  }));

  beforeEach(() => {
    farmIdServiceStub.getNivafarmId$.and.returnValue(of(10));
    fixture = TestBed.createComponent(FarmIdModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
