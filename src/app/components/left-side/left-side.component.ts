/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FarmIdModalComponent } from './farm-id-modal/farm-id-modal.component';
import { NivaTask } from './interfaces/niva-task.class';

@Component({
  selector: 'app-left-side',
  templateUrl: './left-side.component.html',
  styleUrls: ['./left-side.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LeftSideComponent {
  @Input() public tasks$: Observable<NivaTask[]> = of([]);
  @Input() public approvedTasks$: Observable<NivaTask[]> = of([]);
  @Output() public taskSelect = new EventEmitter<NivaTask>();
  @ViewChild('modal', { static: false }) private modal: FarmIdModalComponent;

  public onTaskSelect(task: NivaTask) {
    this.taskSelect.emit(task);
  }

  public openModal() {
    this.modal.open();
  }
}
