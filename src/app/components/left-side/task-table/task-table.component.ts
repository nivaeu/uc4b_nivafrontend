import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable, of } from 'rxjs';
import { NivaTask } from "@app/components/left-side/interfaces/niva-task.class";

/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
@Component({
  selector: 'app-task-table',
  templateUrl: './task-table.component.html',
  styleUrls: ['./task-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskTableComponent implements OnInit {
  @Input() public title = '';
  @Input() public tasks$: Observable<NivaTask[]> = of([]);
  @Input() public showSelected = true;
  @Output() public taskSelect = new EventEmitter<NivaTask>();
  public selectedId: number = null;

  public ngOnInit(): void {}

  public onTaskSelect(task: NivaTask) {
    this.selectedId = this.showSelected ? task.id : null;
    this.taskSelect.emit(task);
  }

  public isSelected(task: NivaTask) {
    return task.id === this.selectedId;
  }
}
