/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Injectable } from '@angular/core';
import { OpenLayersMapComponent } from '@app/components/center/openlayers-map/openlayers-map.component';
import { MapConstants } from '@app/core/map/map.constants';
import Feature from 'ol/Feature';
import WKT from 'ol/format/WKT';
import GeometryType from 'ol/geom/GeometryType';
import MapBrowserPointerEvent from 'ol/MapBrowserPointerEvent';
import { toLonLat } from 'ol/proj';
import { ReplaySubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MapService {
  public mapReadySubject: Subject<void> = new ReplaySubject(1);
  public mapReady$ = this.mapReadySubject.asObservable();

  private _map: OpenLayersMapComponent;

  constructor() {}

  public getMap(): OpenLayersMapComponent {
    return this._map;
  }

  public setMap(map: OpenLayersMapComponent) {
    this._map = map;
  }

  public deselectFeatures() {
    this._map.deselectFeatures();
  }

  public addHoverInteraction() {
    return this.getMap().addPointerMoveInteraction();
  }

  public getEventPixel(event: Event) {
    return this.getMap().getMap().getEventPixel(event);
  }

  public showPointerOnMapFeature(ev: MapBrowserPointerEvent) {
    if (!ev || !ev.map) {
      return;
    }

    const pointerOverFieldFeature = ev.map.forEachFeatureAtPixel(ev.pixel, (feature, layer) => {
      if (!layer) {
        return;
      }

      if (feature instanceof Feature) {
        return feature;
      }
    });

    let cursorStyle = '';

    if (pointerOverFieldFeature) {
      cursorStyle = 'pointer';
    }

    const vpElement = ev.map.getViewport();
    const vpHtmlElement = vpElement as HTMLElement;
    vpHtmlElement.style.cursor = cursorStyle;
  }

  public getGeometryTypeFromFeature(feature: Feature): GeometryType {
    const olGeometryType = feature.getGeometry().getType();
    return this.getGeometryType(olGeometryType);
  }

  public getFeatureFromWktString(geometryString: string): Feature {
    const wkt = new WKT();

    return wkt.readFeature(geometryString, {
      dataProjection: MapConstants.dataProjection,
      featureProjection: MapConstants.mapProjection,
    });
  }

  public getLongitudeAndLatitudeFromCoordinate(coordinate: [number, number]) {
    return toLonLat(coordinate);
  }

  /**
   * Function return wkt representation of the provided feature geometry
   * @param feature Feature to get wkt representation from
   */
  public getWktFromFeature(feature: Feature): string {
    const wkt = new WKT();
    return wkt.writeFeature(feature, {
      dataProjection: MapConstants.dataProjection,
      featureProjection: MapConstants.mapProjection,
    });
  }

  public getFeatureFromWkt(wktString: string) {
    const wkt = new WKT();
    return wkt.readFeature(wktString, {
      dataProjection: MapConstants.dataProjection,
      featureProjection: MapConstants.mapProjection,
    });
  }

  private getGeometryType(geometry: string): GeometryType {
    // get expected geometrytype from geometry
    switch (geometry) {
      case 'Polygon':
        return GeometryType.POLYGON;
      case 'LineString':
        return GeometryType.LINE_STRING;
      case 'Point':
        return GeometryType.POINT;
    }
  }
}
