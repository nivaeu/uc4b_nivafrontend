/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
export class MapConstants {
  // projections
  public static readonly mapProjection = 'EPSG:3857';
  public static readonly dataProjection = 'EPSG:4326';

  public static POINTEMPTY = 'POINT EMPTY';
}
