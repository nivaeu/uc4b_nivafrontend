/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  private _farmId$ = new BehaviorSubject<number>(0);
  private _harvestYear$ = new BehaviorSubject<number>(2021);

  public constructor(private storageService: StorageService) {}

  public set farmId(farmId: number) {
    this._farmId$.next(farmId);
    this.storageService.setNivaFarmId(farmId);
  }

  public set harvestYear(harvestYear: number) {
    this._harvestYear$.next(harvestYear);
    this.storageService.setNivaHarvestYear(harvestYear);
  }

  public getNivafarmId$() {
    return this._farmId$.pipe(
      map((farmId) => {
        if (farmId === 0) {
          const storedFarmId = this.storageService.getNivaFarmId();
          if (storedFarmId) {
            farmId = storedFarmId;
          }
        }
        return farmId;
      })
    );
  }

  public getNivaHarvestYear$() {
    return this._harvestYear$.pipe(
      map((harvestYear) => {
        if (harvestYear === 0) {
          const storedHarvestYear = this.storageService.getNivaHarvestYear();
          if (storedHarvestYear) {
            harvestYear = storedHarvestYear;
          }
        }
        return harvestYear;
      })
    );
  }
}
