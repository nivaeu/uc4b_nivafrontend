/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
/* tslint:disable:no-unused-variable */

import { Mock, SpyHelper } from '@app/helpers/test/spy-helper';
import { StorageService } from '../storage/storage.service';
import { LocalStorageService } from './local-storage.service';

describe('Service: FarmId', () => {
  let storageServiceStub: Mock<StorageService>;
  let farmIdService: LocalStorageService;
  const farmId = 10;
  const harvestYear = 2019;

  beforeEach(() => {
    storageServiceStub = SpyHelper.createMagicalMock(StorageService);
    storageServiceStub.getNivaFarmId.and.returnValue(10);
    farmIdService = new LocalStorageService(storageServiceStub);
  });

  describe('getNivafarmId$', () => {
    it('should return FarmId', (done) => {
      farmIdService.farmId = farmId;
      farmIdService.getNivafarmId$().subscribe((farm) => {
        expect(farm).toBe(farmId);
        done();
      });
    });
  });
  describe('getNivaHarvestYear$', () => {
    it('should return HarvestYear', (done) => {
      farmIdService.harvestYear = harvestYear;
      farmIdService.getNivaHarvestYear$().subscribe((year) => {
        expect(year).toBe(harvestYear);
        done();
      });
    });
  });
});
