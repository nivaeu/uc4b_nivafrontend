/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ECropReport } from '@app/components/left-side/interfaces/e-crop-report-message.class';
import { ECROPReportDto } from '@app/components/left-side/interfaces/e-crop-report-message.dto';
import {
  NivaECropReceipt,
  NivaEcropReturnMessage,
  NivaTask,
  NivaTaskDTO,
  NivaValidation,
} from '@app/components/left-side/interfaces/niva-task.class';
import { ECropReceiptDTO, ECROPReturnMessageDTO, VraValidationDTO } from '@app/components/left-side/interfaces/vra-task.dto';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { AccessTokenService } from '../authentication/access-token.service';
import { AuthService } from '../authentication/auth.service';
import { EndpointsService } from '../endpoints/endpoints.service';
import { LocalStorageService } from '../local-storage/local-storage.service';

@Injectable({
  providedIn: 'root',
})
export class TasksService {
  private farmId = this.localStorageService.farmId;
  private harvestYear = this.localStorageService.harvestYear;

  constructor(
    private http: HttpClient,
    private accessTokenService: AccessTokenService,
    private endpoints: EndpointsService,
    private authService: AuthService,
    private localStorageService: LocalStorageService
  ) {
    this.localStorageService.getNivafarmId$().subscribe((farmId) => {
      this.farmId = farmId;
    });
    this.localStorageService.getNivaHarvestYear$().subscribe((harvestYear) => {
      this.harvestYear = harvestYear;
    });
  }

  public getTasks(farmId: number, harvestYear: number): Observable<NivaTask[]> {
    return this.getValidAccessToken$().pipe(
      switchMap((encodedAccessToken) => {
        return this.http.get<NivaTaskDTO[]>(
          `${this.endpoints.nivaConnectorApi}/api/proprietary/seges/datahub/farm/${farmId}/harvestYear/${harvestYear}/tasks`,
          {
            headers: {
              Authorization: `Bearer ${encodedAccessToken}`,
            },
          }
        );
      }),
      map((nivaTaskDtos: NivaTaskDTO[]) => nivaTaskDtos.map((nivaTaskDto) => new NivaTask(nivaTaskDto)))
    );
  }

  public getECropReport(id: number) {
    return this.getValidAccessToken$().pipe(
      switchMap((encodedAccessToken) => {
        return this.http.get<ECROPReportDto>(
          `${this.endpoints.nivaConnectorApi}/api/proprietary/seges/datahub/prescriptionmaps/applied/${id}?farmId=${this.farmId}&harvestYear=${this.harvestYear}&language=en`,
          {
            headers: {
              Authorization: `OAuth ${encodedAccessToken}`,
            },
          }
        );
      }),
      map((eCROPReportDto) => {
        // Adds the specifiedCropPlots from mock ECrop to the received ECrop. This is done to have both point and polygon data for testing.
        // eCROPReportDto.ECROPReportMessage.AgriculturalProducerParty.AgriculturalProductionUnit.SpecifiedCropPlot.push(...this.eCropMessageDtoMock.AgriculturalProducerParty.AgriculturalProductionUnit.SpecifiedCropPlot)
        return new ECropReport(eCROPReportDto);
      })
    );
  }

  public checkTaskValidity(task: NivaTask): Observable<NivaValidation[]> {
    return this.getValidAccessToken$().pipe(
      switchMap((encodedAccessToken) => {
        return this.http.get<VraValidationDTO[]>(
          `${this.endpoints.nivaConnectorApi}/api/proprietary/seges/datahub/prescriptionmaps/applied/${task.id}/ecropvalidation?farmId=${this.farmId}&harvestYear=${this.harvestYear}&language=en`,
          {
            headers: {
              Authorization: `OAuth ${encodedAccessToken}`,
            },
          }
        );
      }),
      map((vraValidationDTOs: VraValidationDTO[]) => vraValidationDTOs.map((vraValidationDTO) => new NivaValidation(vraValidationDTO)))
    );
  }

  public getECropReceipt(task: NivaTask): Observable<NivaECropReceipt> {
    return this.getValidAccessToken$().pipe(
      switchMap((encodedAccessToken) => {
        return this.http.get<ECropReceiptDTO>(
          `${this.endpoints.nivaConnectorApi}/api/proprietary/seges/datahub/prescriptionmaps/applied/${task.id}/ecropreceipt?farmId=${this.farmId}&harvestYear=${this.harvestYear}&language=en`,
          {
            headers: {
              Authorization: `OAuth ${encodedAccessToken}`,
            },
          }
        );
      }),
      map((eCropReceiptDTO) => new NivaECropReceipt(eCropReceiptDTO)),
      catchError((error) => {
        return of(new NivaECropReceipt({ Sender: '', Purpose: '', Recipient: '', CropPlot: [], error: error }));
      })
    );
  }

  // Get method because all the hard 'send' work is done in backend
  public sendTaskToPA(task: NivaTask) {
    return this.getValidAccessToken$().pipe(
      switchMap((encodedAccessToken) => {
        return this.http.get<ECROPReturnMessageDTO>(
          `${this.endpoints.nivaConnectorApi}/api/proprietary/seges/datahub/prescriptionmaps/applied/${task.id}/payingagency/nl?farmId=${this.farmId}&harvestYear=${this.harvestYear}&language=en`,
          {
            headers: {
              Authorization: `OAuth ${encodedAccessToken}`,
            },
          }
        );
      }),
      map((eCROPReturnMessageDTO) => {
        // if eCROPReturnMessageDTO is returned it is because of an errormessage
        if (eCROPReturnMessageDTO) {
          return new NivaEcropReturnMessage(eCROPReturnMessageDTO);
        } else {
          return {
            messageCount: 0,
            messages: [],
            returnCode: 0,
            returnMessage: 'Task has been successfully sent to the Paying Agency',
            timeStamp: new Date(),
          } as NivaEcropReturnMessage;
        }
      })
    );
  }

  private getValidAccessToken$() {
    const accessToken = this.accessTokenService.accessToken;
    if (accessToken === undefined || this.accessTokenService.hasExpired(accessToken)) {
      return this.authService.beginAuthenticationFlow().pipe(
        filter((isAuthenticated) => isAuthenticated),
        switchMap(() => this.accessTokenService.getEncodedAccessToken())
      );
    } else {
      return of(this.accessTokenService.getEncodedAccessToken());
    }
  }
}
