/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { inject, TestBed } from '@angular/core/testing';
import { StorageService } from '@app/core/storage/storage.service';
import { WindowRefService } from '@app/core/window/window-ref.service';
import { WindowRefServiceMock } from '@app/core/window/window-ref.service.mock';

describe('Storage Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StorageService, { provide: WindowRefService, useValue: WindowRefServiceMock }],
    });
  });

  it('should be created', inject([StorageService], (service: StorageService) => {
    expect(service).toBeTruthy();
  }));
});
