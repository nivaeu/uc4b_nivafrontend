/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { IStorageService } from '@app/core/storage/storage.service';

export class StorageServiceMock implements IStorageService {
  public getNivaFarmId(): number {
    return 0;
  }
  public setNivaFarmId(nivaFarmId: number): void {}
  public removeNivaFarmId(): void {}
  public removeSelectedFarmIds() {}
  public getSelectedFarmIds(): number[] {
    return [1, 2, 3];
  }
  public setSelectedFarmIds(selectedFarmIds: number[]) {}
  public saveToStorage<T>(key: string, value: T) {}
  public getFromStorage<T>(key: string): T {
    return {} as T;
  }
  public removeFromStorage(key: string): void {}
}
