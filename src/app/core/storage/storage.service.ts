/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Injectable } from '@angular/core';

export interface IStorageService {
  getFromStorage<T>(key: string): T;
  saveToStorage<T>(key: string, value: T): void;
  removeFromStorage(key: string): void;
  getNivaFarmId(): number;
  setNivaFarmId(nivaFarmId: number): void;
  removeNivaFarmId(): void;
}

@Injectable({
  providedIn: 'root',
})
export class StorageService implements IStorageService {
  private prefix = 'NivaConnector';

  /**
   * Saves data to localstorage.
   * Removes existing data with same name first.
   */
  public saveToStorage(key: string, value: any) {
    this.setValue(key, value);
  }

  /**
   * Gets specific data from localstorage.
   */
  public getFromStorage<T>(key: string): T {
    return this.getValue(key);
  }

  /**
   * Remove key and value in localestorage
   */
  public removeFromStorage(key: string): void {
    this.remove(key);
  }

  public getNivaFarmId(): number {
    return this.getValue(this.prefixedKey('nivaFarmId'));
  }

  public setNivaFarmId(farmId: number) {
    this.setValue('nivaFarmId', farmId);
  }

  public removeNivaFarmId() {
    this.remove('nivaFarmId');
  }

  public getNivaHarvestYear(): number {
    return this.getValue(this.prefixedKey('nivaHarvestYear'));
  }

  public setNivaHarvestYear(harvestYear: number) {
    this.setValue('nivaHarvestYear', harvestYear);
  }

  public removeNivaHarvestYear() {
    this.remove('nivaHarvestYear');
  }

  private setValue(key: string, value: any) {
    const serialized = JSON.stringify(value);

    localStorage.setItem(this.prefixedKey(key), serialized);
  }

  private getValue(key: string) {
    try {
      const serialized = localStorage.getItem(this.prefixedKey(key));
      const deserialized = JSON.parse(serialized);

      return deserialized;
    } catch (e) {
      return undefined;
    }
  }

  private remove(key: any) {
    localStorage.removeItem(this.prefixedKey(key));
  }

  private prefixedKey(key: string) {
    if (key.includes(`${this.prefix}.`)) {
      return key;
    }

    const prefixedKey = `${this.prefix}.${key}`;

    return prefixedKey;
  }
}
