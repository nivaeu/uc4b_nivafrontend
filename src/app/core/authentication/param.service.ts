/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Injectable, Injector } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import trimStart from 'lodash-es/trimStart';

export interface IParamService {
  getParam(param: string): any;
}

@Injectable({
  providedIn: 'root',
})
export class ParamService implements IParamService {
  constructor(private injector: Injector) {}

  public getParam(param: string, defaultValue?: any) {
    const fromRouteParams = this.getParamFromRouteParams(param);
    const fromRouteQueryString = this.getParamFromRouteQueryString(param);
    const fromQueryString = this.getParamFromQueryString(param);
    const fromHash = this.getParamFromHash(param);
    const value = fromRouteParams || fromRouteQueryString || fromQueryString || fromHash || defaultValue || undefined;

    return value;
  }

  private getActivatedRoute() {
    try {
      const route = this.injector.get(ActivatedRoute);

      return route;
    } catch (error) {
      // tslint:disable-next-line:no-console
      console.error(error);

      return undefined;
    }
  }

  private getParamFromRouteParams(param: string): string {
    const route = this.getActivatedRoute();

    if (!route || !route.snapshot || !route.snapshot.params) {
      return undefined;
    }

    return route.snapshot.params[param];
  }

  private getParamFromRouteQueryString(param: string): string {
    const route = this.getActivatedRoute();

    if (!route || !route.snapshot || !route.snapshot.queryParams) {
      return undefined;
    }

    return route.snapshot.queryParams[param];
  }

  private getParamFromQueryString(param: string) {
    return this.parseQueryStringAndGetParam(trimStart(location.search || '', '?'), param);
  }

  private getParamFromHash(param: string) {
    const hash = location.hash || '';

    if (!hash) {
      return undefined;
    }

    const hashSplit = hash.split('?');

    if (hashSplit.length < 2) {
      return undefined;
    }

    return this.parseQueryStringAndGetParam(hashSplit[1], param);
  }

  private parseQueryStringAndGetParam(str: string, param: string) {
    if (!str) {
      return undefined;
    }

    const queryStringMap = this.parseQueryString(str);

    return queryStringMap[param];
  }

  private parseQueryString(str: string) {
    if (!str) {
      return {};
    }

    return str
      .split('&')
      .map((s) => s.split('='))
      .reduce(
        (obj, current) => ({ ...obj, [current[0]]: current[1] }),
        {} as {
          [key: string]: string;
        }
      );
  }
}
