/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { DOCUMENT } from '@angular/common';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Inject, Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { AuthStateService } from '@app/core/authentication/auth-state.service';
import { utc } from 'moment';
import { interval, Observable, of, Subscription } from 'rxjs';
import { catchError, filter, first, map, switchMap, take, tap } from 'rxjs/operators';
import { AccessTokenService } from './access-token.service';
import { AuthUrlsService } from './auth-urls.service';
import { ParamService } from './param.service';

export interface IAuthService {
  beginAuthenticationFlow(): Observable<boolean>;
  logout(): void;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService implements IAuthService {
  private timerSubscription: Subscription;

  constructor(
    private accessTokenService: AccessTokenService,
    private paramService: ParamService,
    private http: HttpClient,
    @Inject(DOCUMENT) private document: any,
    private authUrlsService: AuthUrlsService,
    private authStateService: AuthStateService,
    private injector: Injector
  ) {}

  public beginAuthenticationFlow() {
    return this.runAuthenticationFlow().pipe(
      take(1),
      tap((isAuthenthenticated) => this.authStateService.isAuthenticated$.next(isAuthenthenticated))
    );
  }

  public logout() {
    this.redirectToLogout();
  }

  public login(redirectUrl?: string) {
    this.redirectToLogin(redirectUrl);
  }

  public renewAccessToken() {
    return this.http
      .get<any>(this.authUrlsService.renewUrl, { withCredentials: true })
      .pipe(
        tap((response) => this.storeOrClearToken(response)),
        map((response) => {
          return response.status === 200;
        })
      );
  }

  private runAuthenticationFlow() {
    const firstToken = this.paramService.getParam('token');
    const tokenDefined = !!this.accessTokenService.accessToken && !!this.paramService.getParam('cmredirect');

    if (tokenDefined) {
      this.startRenewLoop();
    }

    if (firstToken) {
      this.accessTokenService.storeEncodedAccessToken(firstToken);
      return of(true);
    } else {
      const token = this.accessTokenService.accessToken;

      if (!token) {
        this.login(location.href);
        return of(false);
      }

      if (this.accessTokenService.hasExpired(token) || this.accessTokenService.tokenWillExspireBeforeRenew(token)) {
        return this.renew().pipe(
          filter((isAuthenticated) => isAuthenticated),
          map(() => {
            this.startRenewLoop();

            return true;
          })
        );
      }
      return of(true);
    }
  }

  private startRenewLoop() {
    if (!this.timerSubscription) {
      this.timerSubscription = this.getRenewTimer().subscribe();
    }
  }

  private getRenewTimer() {
    let renewOffset = this.accessTokenService.getRenewTimerOffset(this.accessTokenService.accessToken);

    if (renewOffset <= 0) {
      const exp = this.accessTokenService.getExpFromToken(this.accessTokenService.accessToken);
      // tslint:disable-next-line:no-console
      console.error(`Renew offset was under 0. Time is ${utc().unix()} and exp time from token is ${exp}`);

      renewOffset = this.accessTokenService.periodUntilTokenWillBeRenewed();
      // tslint:disable-next-line:no-console
      console.log(`Setting default renew offset of ${renewOffset}`);
    }

    return interval(renewOffset).pipe(switchMap(() => this.renew()));
  }

  private renew() {
    return this.renewAccessToken().pipe(
      first(),
      catchError((err) => this.handleRenewError(err)),
      tap((isAuthenticated) => {
        this.authStateService.isAuthenticated$.next(isAuthenticated);
      })
    );
  }

  private handleRenewError(err: any) {
    this.redirectToLogin();

    return of(false);
  }

  private storeOrClearToken(response: HttpResponse<any>) {
    return response.status === 200
      ? this.accessTokenService.storeEncodedAccessToken(response.body)
      : this.accessTokenService.clearAccessToken();
  }

  private redirectToLogin(redirectUrl?: string) {
    this.clearTokenAndRedirectTo(this.authUrlsService.loginUrl(redirectUrl));
  }

  private redirectToLogout() {
    this.clearTokenAndRedirectTo(this.authUrlsService.logoutUrl);
  }

  private redirectToLandingPage() {
    const router = this.injector.get(Router);
    let url = this.document.URL;
    const hasRedirect = url.substring(url.indexOf('?')).indexOf('cmredirect=');

    if (hasRedirect >= 0) {
      // Extract the redirect url and use it again, so we don't chain redirects.
      const start = url.indexOf('?') + url.substring(url.indexOf('?')).indexOf('cmredirect=') + 'cmredirect='.length;
      const end = url.substring(start).indexOf('&');
      url = url.substring(start, end > 0 ? end : undefined);
      router.navigate(['/'], { queryParams: { cmredirect: decodeURIComponent(url) } });
    } else {
      router.navigate(['/'], { queryParams: { cmredirect: url } });
    }
  }

  private clearTokenAndRedirectTo(url: string) {
    this.accessTokenService.clearAccessToken();
    this.document.location.assign(url);
  }
}
