/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Injectable } from '@angular/core';
import get from 'lodash-es/get';
import isString from 'lodash-es/isString';
import { utc } from 'moment';

export interface AccessToken {
  header: { [key: string]: any };
  payload: { [key: string]: any };
}

export const ACCESS_TOKEN_KEY = 'accesstoken';

export interface IAccessTokenService {
  readonly accessToken: AccessToken;
  getEncodedAccessToken(): string;
  storeEncodedAccessToken(token: string): void;
  clearAccessToken(): void;
  getSecondsUntilExpiry(token: AccessToken): number;
  getMilliSecondsUntilExpiry(token: AccessToken): number;
  getRenewTimerOffset(token: AccessToken): number;
  hasExpired(token: AccessToken): boolean;
  tokenWillExspireBeforeRenew(token: AccessToken): boolean;
  periodUntilTokenWillBeRenewed(): number;
}

@Injectable({
  providedIn: 'root',
})
export class AccessTokenService implements IAccessTokenService {
  private clockDiff = 0;
  private renewTokenMilliSecondsBeforeExpiry = 300000; // 5 minutes

  private get storedAccessToken() {
    const token = localStorage.getItem(ACCESS_TOKEN_KEY);

    if (token === null || !token) {
      return undefined;
    }

    return token;
  }

  private set storedAccessToken(value: string) {
    if (!value) {
      localStorage.removeItem(ACCESS_TOKEN_KEY);
    } else {
      localStorage.setItem(ACCESS_TOKEN_KEY, value);
    }
  }

  private calculateAndSetClockSkew() {
    const nbf = this.getNbfFromToken(this.accessToken);
    const now = this.getNowInSeconds();
    this.clockDiff = nbf - now;
  }

  public get accessToken(): AccessToken {
    try {
      const token = this.storedAccessToken;

      if (!token) {
        return undefined;
      }

      const parsedToken = this.parseJwtToken(token);

      return parsedToken;
    } catch (error) {
      // tslint:disable-next-line:no-console
      console.error(error);

      return undefined;
    }
  }

  public getEncodedAccessToken() {
    return this.storedAccessToken;
  }

  public storeEncodedAccessToken(token: string) {
    this.storedAccessToken = token;
    this.calculateAndSetClockSkew();
  }

  public clearAccessToken() {
    this.storedAccessToken = undefined;
  }

  public getSecondsUntilExpiry(token: AccessToken, regulatedNow: number = this.getNowInSeconds()) {
    const exp = this.getExpFromToken(token);
    const diff = exp - regulatedNow;

    return diff < 0 ? 0 : diff;
  }

  public getMilliSecondsUntilExpiry(token: AccessToken, regulatedNow?: number) {
    return this.getSecondsUntilExpiry(token, regulatedNow) * 1000;
  }

  /**
   * Gets milliseconds until expiry minus renewMilliSecondsBeforeExpiry.
   */
  public getRenewTimerOffset(token: AccessToken) {
    const now = this.getNowInSeconds();
    const regulatedNowInSeconds = this.getNowInSecondsRegulated(now);
    const milliSecondsUntilExpiry = this.getMilliSecondsUntilExpiry(token, regulatedNowInSeconds);
    return milliSecondsUntilExpiry - this.renewTokenMilliSecondsBeforeExpiry;
  }

  public hasExpired(token: AccessToken, now?: number) {
    const nbf = this.getNbfFromToken(token);
    const internalNow = now || this.getNowInSeconds();

    const nowRegulatedForClockScrew = this.getNowInSecondsRegulated(internalNow);
    const isNowAfterNbf = nbf <= nowRegulatedForClockScrew;
    const secondsUntilExpiredBelowZero = this.getSecondsUntilExpiry(token, nowRegulatedForClockScrew) <= 0;
    return isNowAfterNbf && secondsUntilExpiredBelowZero;
  }

  public getNbfFromToken(token: AccessToken) {
    return this.getPayloadNumberValue('nbf', token);
  }

  public getExpFromToken(token: AccessToken) {
    return this.getPayloadNumberValue('exp', token);
  }

  public parseJwtToken(token: string): AccessToken {
    if (!token && !isString(token)) {
      throw new Error('No token or token is not a string');
    }

    const tokenSplit = token.split('.');

    if (tokenSplit.length < 2) {
      throw new Error('Unable to split string at "."-character');
    }

    const header = JSON.parse(atob(tokenSplit[0]));
    const payload = JSON.parse(atob(tokenSplit[1]));

    return { header, payload };
  }

  private getNowInSecondsRegulated(now: number) {
    return now + this.clockDiff;
  }

  public getNowInSeconds() {
    return utc().unix();
  }

  public tokenWillExspireBeforeRenew(token: AccessToken) {
    return (
      this.getTokenValidityPeriodInMilliSeconds() - this.renewTokenMilliSecondsBeforeExpiry / 2 > this.getMilliSecondsUntilExpiry(token)
    );
  }

  public periodUntilTokenWillBeRenewed() {
    return this.getTokenValidityPeriodInMilliSeconds() - this.renewTokenMilliSecondsBeforeExpiry;
  }

  private getTokenValidityPeriodInMilliSeconds() {
    const validityPeriod = (this.getExpFromToken(this.accessToken) - this.getNbfFromToken(this.accessToken)) * 1000;
    return validityPeriod;
  }

  private getPayloadNumberValue(key: string, token: AccessToken) {
    const { payload } = token;

    if (!payload) {
      return undefined;
    }

    const value = get(payload, key);
    const asNumber = parseInt(value.toString(), 10);

    if (!isFinite(asNumber)) {
      return undefined;
    }

    return asNumber;
  }
}
