/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Injectable } from '@angular/core';
import { EndpointsService } from '@app/core/endpoints/endpoints.service';
import { environment } from 'environments/environment';
import trimEnd from 'lodash-es/trimEnd';

@Injectable({
  providedIn: 'root',
})
export class AuthUrlsService {
  constructor(private endpointsService: EndpointsService) {}
  // Don´t remove double encoding of 'returnTo' url parameter. It is required because
  // ADFS login endpoint is deleting url params in returnUrl if it is not double encoded

  public loginUrl(redirectUrl?: string) {
    return `${this.endpointsService.plantAuthorizationService}/login/login?caller=${environment.auth.callerId}&application=${
      environment.auth.applicationId
    }&redirect=${this.encodedReturnUrl(redirectUrl)}`;
  }

  public get logoutUrl() {
    return `${this.endpointsService.plantAuthorizationService}/login/logout?caller=${environment.auth.callerId}&redirect=${location.origin}`;
  }

  public get renewUrl() {
    return `${this.endpointsService.plantAuthorizationService}/api/renew/${environment.auth.callerId}/${environment.auth.applicationId}`;
  }

  private encodedReturnUrl(redirectUrl?: string) {
    const url = encodeURIComponent(location.href);
    const origin = trimEnd(location.origin, '/');
    const path = trimEnd(location.pathname, '/');

    // The {accesstoken} symbol in url is a placeholder, where the backend will put the initial jwt token
    return encodeURIComponent(`${origin}/${path}/#/token?token={accesstoken}&returnTo=${encodeURIComponent(url)}`);
  }
}
