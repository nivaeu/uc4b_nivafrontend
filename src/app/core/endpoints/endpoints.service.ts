/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import trimEnd from 'lodash-es/trimEnd';

@Injectable({
  providedIn: 'root',
})
export class EndpointsService {
  get plantApi() {
    return trimEnd(environment.endpoints.plantApi, '/');
  }
  get plantAuthorizationService() {
    return trimEnd(environment.endpoints.plantAuthorizationService, '/');
  }
  get nivaConnectorApi() {
    return trimEnd(environment.endpoints.nivaConnectorApi, '/');
  }
  get nivaValidatorApi() {
    return trimEnd(environment.endpoints.nivaValidatorApi, '/');
  }
}
