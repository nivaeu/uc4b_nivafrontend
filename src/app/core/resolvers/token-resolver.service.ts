/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { ParamService } from '@app/core/authentication/param.service';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TokenResolverService implements Resolve<any> {
  constructor(private paramService: ParamService, private router: Router) {}

  public resolve(): Observable<any> {
    const returnTo = this.paramService.getParam('returnTo');
    const decodedReturnTo = decodeURIComponent(returnTo);

    if (returnTo) {
      location.assign(decodedReturnTo);
    } else {
      this.router.navigate(['']);
    }

    return of(true);
  }
}
