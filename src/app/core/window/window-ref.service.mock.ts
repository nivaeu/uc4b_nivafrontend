/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { IWindowRefService } from '@app/core/window/window-ref.service';
import { IWindow } from './window.interface';

export const WindowRefServiceMock: IWindowRefService = {
  get nativeWindow(): IWindow {
    return {
      config: {},
      location: {
        href: '',
      },
      localStorage: {
        getItem: (name: string) => {
          /* */
        },
        setItem: (name: string, data: any) => {
          /* */
        },
        removeItem: (name: string) => {
          /* */
        },
        clear: () => {},
      },
      sessionStorage: {
        getItem: (name: string) => {
          /* */
        },
        setItem: (name: string, data: any) => {
          /* */
        },
        removeItem: (name: string) => {
          /* */
        },
      },
    } as IWindow;
  },
};
