/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Injectable } from '@angular/core';
import keys from 'lodash-es/keys';
import { IWindow } from './window.interface';

export interface IWindowRefService {
  nativeWindow: IWindow;
}

function getWindow(shouldDecorate: boolean): any {
  const w = (window as any) as IWindow;

  if (shouldDecorate && w.config) {
    const copy = { ...w.config };

    keys(w.config)
      .filter((key) => ['env'].indexOf(key) === -1) // Whitelist some keys...
      .forEach((key) =>
        Object.defineProperty(w.config, key, {
          get: function () {
            return copy[key];
          },
        })
      );
  }

  return w;
}

@Injectable({
  providedIn: 'root',
})
export class WindowRefService implements IWindowRefService {
  private shouldDecorate = true;

  public get nativeWindow(): IWindow {
    const window = getWindow(this.shouldDecorate);

    this.shouldDecorate = false;

    return window;
  }

  public get localStorage() {
    return this.nativeWindow.localStorage;
  }
}
