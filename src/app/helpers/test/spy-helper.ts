/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
 */
import { Provider, Type } from '@angular/core';

export type Mock<T> = T & { [P in keyof T]: T[P] & jasmine.Spy };
/*
  This class provides helpers for easy mocking with spies
*/
export class SpyHelper {
  /*
    Ensures typesafe creation of a spy object
  */
  public static createSpyObj<T>(name: string = 'spyObject', methods: Array<keyof T>): jasmine.SpyObj<T> {
    return jasmine.createSpyObj<T>(name, methods as any);
  }

  public static createMagicalMock<T>(type: Type<T>): Mock<T> {
    const target: any = {};

    function installProtoMethods(proto: any) {
      if (proto === null || proto === Object.prototype) {
        return;
      }

      for (const key of Object.getOwnPropertyNames(proto)) {
        // tslint:disable-next-line:no-non-null-assertion
        const descriptor = Object.getOwnPropertyDescriptor(proto, key)!; // assert not null

        if (typeof descriptor.value === 'function' && key !== 'constructor') {
          target[key] = jasmine.createSpy(key);
        }
      }

      installProtoMethods(Object.getPrototypeOf(proto));
    }

    installProtoMethods(type.prototype);

    return target;
  }

  public static provideMagicalMock<T>(type: Type<T>): Provider {
    return {
      provide: type,
      useFactory: () => SpyHelper.createMagicalMock(type),
    };
  }
}
