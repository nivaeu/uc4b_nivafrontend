var gulp = require('gulp');
var jsonModify = require('gulp-json-modify');

gulp.task('setBaseUrl', function () {
  const env = process.argv[4] // [ 'node', 'path/to/gulp.js', 'taskname', '--env', 'https:localhost.vfltest.dk:4200']

  // Write env to config file
  return gulp.src('./e2e/testcafe/config.json')
    .pipe(jsonModify({
      key: 'baseUrl',
      value: env
    }))
    .pipe(gulp.dest('./e2e/testcafe/', {
      overwrite: true
    }));
});
