// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

const karmaConf = require('./karma.conf');

module.exports = function (config) {
  config.set({
    ...karmaConf.getDefaultConfig(config),
    browsers: ['Chrome'],
    reporters: ['teamcity', 'coverage-istanbul']
  });
};
