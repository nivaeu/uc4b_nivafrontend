var fs = require('fs');
const filePath = './CHANGELOG.md';
const from = /https:\/\/bitbucket.seges.dk\/scm\/cm\/nivafrontend/g;
const to = 'https://bitbucket.seges.dk/projects/CM/repos/nivafrontend';

fs.readFile(filePath, 'utf8', function (err, data) {
  if (err) {
    return console.log(err);
  }
  var result = data.replace(from, to);

  fs.writeFile(filePath, result, 'utf8', function (err) {
    if (err) return console.log(err);
  });

  console.log(`Replaced ${from} with ${to} in ${filePath}`);
});
