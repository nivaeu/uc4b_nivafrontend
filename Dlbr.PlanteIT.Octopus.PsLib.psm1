﻿Import-Module WebAdministration

function Add-Website{
	param(
	[Parameter(Position=0, Mandatory=$true)]
	[string]$siteName,
	[Parameter(Position=1, Mandatory=$true)]
	[AllowEmptyString()]
	[string]$appPoolName,
	[Parameter(Position=2, Mandatory=$true)]
	[string]$webRoot,
	[Parameter(Position=3, Mandatory=$true)]
	[string]$hostHeader,
	[Parameter(Position=4, Mandatory=$true)]
	[string]$port,
	[Parameter(Position=5, Mandatory=$true)]
	[string]$id
	)

	# Delete exiting website
	$sitePath = ("IIS:\Sites\" + $siteName)
	$site = Get-Item $sitePath -ErrorAction SilentlyContinue

  Write-Host "Creating Website with site: $siteName, name: $applicationName, path: $sitePath, applicationPool: $appPoolName";
  $siteBindings = ":" + $port + ":" + $hostHeader

  new-item $sitePath -bindings @{protocol="http";bindingInformation=$siteBindings} -id $id -physicalPath $webRoot -Force

  Set-ItemProperty $sitePath -name applicationPool -value $appPoolName
}

function Delete-Website{
	param(
	[Parameter(Position=0, Mandatory=$true)]
	[string]$siteName
	)

	# Delete exiting website
	$sitePath = ("IIS:\Sites\" + $siteName)

	# Bruger try-catch pga. noget "timing issue with loading the assemblies in the Powershell Host" (http://help.octopusdeploy.com/discussions/problems/5172-error-using-get-website-in-predeploy-because-of-filenotfoundexception)
	Try{
		$site = Get-Item $sitePath -ErrorAction SilentlyContinue
	} Catch [System.IO.FileNotFoundException]{
		$site = Get-Item $sitePath -ErrorAction SilentlyContinue
	}

	if ($site) {

		#Finder appPool til sitet
		$appPool = Get-Item $sitePath | Select-Object applicationPool -ErrorAction SilentlyContinue
		if ($appPool) {
			$siteAppPoolName = $appPool.applicationPool
		}

		Write-Host "Stopper website $siteName"
		Stop-Website -Name $siteName

		Write-Host "Sletter website $siteName"
		Remove-WebSite -Name $siteName

		if ($siteAppPoolName) {

			Delete-AppPool $siteAppPoolName
		}
	}
}

function Delete-AppPool{
	param(
	[Parameter(Position=0, Mandatory=$true)]
	[string]$appPoolName
	)

		$appPoolPath = ("IIS:\AppPools\" + $appPoolName)
		$pool = Get-Item $appPoolPath -ErrorAction SilentlyContinue
		if ($pool) {
			Write-Host "Recycler appPool $appPoolName"
			Restart-WebItem IIS:\AppPools\$appPoolName
			Write-Host "Sletter appPool $appPoolName"
			Remove-Item -Path IIS:\AppPools\$appPoolName -Force -Recurse
		}

}

function Delete-FolderContent{
	param(
	[Parameter(Position=0, Mandatory=$true)]
	[string]$webDir,
	[Parameter(Position=1, Mandatory=$false)]
	[string[]]$excludeItems
	)

	if (Test-Path -path $webDir) {
        Get-ChildItem $webDir -Exclude $excludeItems |
        Remove-Item -Recurse -Force

		if (Test-Path -path $webDir\* -exclude $excludeItems) {
			Write-Host "$webDir blev IKKE tømt helt efter"
	    }
	} else {
		Write-Host "Folderen $webDir eksisterer ikke - opret den"
		new-item $webDir -itemtype directory
	}
}

function Add-AppPool {
	param(
	[Parameter(Position=0, Mandatory=$true)]
	[string]$appPoolName,
	[Parameter(Position=1, Mandatory=$true)]
	[string]$appPoolFrameworkVersion,
	[Parameter(Position=2, Mandatory=$true)]
	[string]$appPoolIdentity
	)
	write-host "Configuring app pool $appPoolName, with framework v$appPoolFrameworkVersion and user $appPoolIdentity"
	cd IIS:\
	$appPoolPath = ("IIS:\AppPools\" + $appPoolName)
	$pool = Get-Item $appPoolPath -ErrorAction SilentlyContinue

	if($pool) {
		write-host "Deleting existing appPool: $appPoolPath"
		Remove-Item $appPoolPath -Force -Recurse
	}

	if ( !(Test-Path $appPoolPath) ) {
		Write-Host "App pool $appPoolName does not exist. Creating..."
		$newAppPool = New-Item $appPoolPath
		$newAppPool.managedRuntimeVersion = $appPoolFrameworkVersion
		$newAppPool.processModel.identityType = $appPoolIdentity
		$newAppPool.processModel.idleTimeout = [TimeSpan] "0.00:00:00"
		$newAppPool.recycling.periodicRestart.time = [TimeSpan] "00:00:00"

		$newAppPool | Set-Item

		clear-ItemProperty IIS:\AppPools\$appPoolName -Name Recycling.periodicRestart.schedule
		set-ItemProperty IIS:\AppPools\$appPoolName -Name Recycling.periodicRestart.schedule -Value @{value="03:00:00"}



	} else {
		Write-Host "App pool $appPoolName already exists."
	}

	cd c:
}

function Add-WebApplication{
	param(
	[Parameter(Position=0, Mandatory=$true)]
	[string]$siteName,
	[Parameter(Position=1, Mandatory=$true)]
	[string]$applicationName,
	[Parameter(Position=2, Mandatory=$true)]
	[string]$sitePath,
	[Parameter(Position=3, Mandatory=$true)]
	[string]$appPoolName
	)

  Write-Host "Creating webApplication with site: $siteName, name: $applicationName, path: $sitePath, applicationPool: $appPoolName";
	New-WebApplication -Site $siteName -Name $applicationName -PhysicalPath "$sitePath" -ApplicationPool $appPoolName > $null -force
}


Write-Host 'Imported Dlbr.PlanteIT.Octopus.PsLib.psm1'
