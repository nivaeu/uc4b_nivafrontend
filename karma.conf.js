// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

/**
 * Get our default Karma config
 *
 * @param {object} karmaConfig Karma config object, we use properties from that object!
 * @returns {object} Object litteral, that can be used with Karma's .set() method.
 */
function getDefaultConfig(karmaConfig) {
  const conf = {
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-spec-reporter'),
      require('karma-chrome-launcher'),
      require('karma-firefox-launcher'),
      require('karma-edge-launcher'),
      require('karma-ie-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma'),
      require('karma-teamcity-reporter')
    ],
    client: {
      clearContext: false, // leave Jasmine Spec Runner output visible in browser
      captureConsole: false,
      runInParent: true,
      jasmine: {
        random: false
      }
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, 'coverage'),
      reports: ['html', 'lcovonly'],
      fixWebpackSourcePaths: true
    },
    specReporter: {
      maxLogLines: 0,
      suppressErrorSummary: false, // do not print error summary
      suppressFailed: false, // do not print information about failed tests
      suppressPassed: true, // do not print information about passed tests
      suppressSkipped: false, // do not print information about skipped tests
      showSpecTiming: true, // print the time elapsed for each spec
      failFast: false // test will finish with error when a first fail occurs.
    },
    files: [
      './node_modules/jquery/dist/jquery.min.js',
      {
        pattern: './src/**/*.json',
        included: false
      }
    ],
    angularCli: {
      environment: 'dev'
    },
    port: 9876,
    colors: true,
    logLevel: karmaConfig && karmaConfig.LOG_INFO ? karmaConfig.LOG_INFO : undefined,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false,
    reporters: ['spec', 'progress', 'kjhtml'],
    processKillTimeout: 120000,
    browserNoActivityTimeout: 140000,
    captureTimeout: 120000
  };

  return conf;
}

function defaultKarmaConfigSetter(config) {
  config.set(getDefaultConfig(config));
}

// Public API
module.exports.getDefaultConfig = getDefaultConfig;
module.exports.default = defaultKarmaConfigSetter;
